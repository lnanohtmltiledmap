#ifndef ULINUX_ARCHS_AARCH64_START_S
#define ULINUX_ARCHS_AARCH64_START_S
#ifdef __GNUAS__
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
	.text
	.global _start
	.type _start, STT_FUNC
	.align 2
_start:
	mov x0, sp
	b ulinux_start
#else
#error "your assembler is not supported for passing the ABI stack (program arguments, environment variables, ELF auxiliary vectors) to an entry point using C calling convention"
#endif
#endif
