#ifndef ULINUX_ARCH_STAT_H
#define ULINUX_ARCH_STAT_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
struct ulinux_stat {
	ulinux_ul dev;		/* Device.  */
	ulinux_ul ino;		/* File serial number.  */
	ulinux_ui mode;		/* File mode.  */
	ulinux_ui nlink;	/* Link count.  */
	ulinux_ui uid;		/* User ID of the file's owner.  */
	ulinux_ui gid;		/* Group ID of the file's group. */
	ulinux_ul rdev;		/* Device number, if device.  */
	ulinux_ul __pad1;
	ulinux_sl size;		/* Size of file, in bytes.  */
	ulinux_si blksize;	/* Optimal block size for I/O.  */
	ulinux_si __pad2;
	ulinux_sl blocks;	/* Number 512-byte blocks allocated. */
	ulinux_sl atime;	/* Time of last access.  */
	ulinux_ul atime_nsec;
	ulinux_sl mtime;	/* Time of last modification.  */
	ulinux_ul mtime_nsec;
	ulinux_sl ctime;	/* Time of last status change.  */
	ulinux_ul ctime_nsec;
	ulinux_ui __unused4;
	ulinux_ui __unused5;
};
#endif
