#ifndef ULINUX_ELF_H
#define ULINUX_ELF_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */

/* reduced version */

/* entry types */
#define ULINUX_AT_NULL		0	/* end of vector */
#define ULINUX_AT_SYSINFO_EHDR	33	/* vdso related */

struct ulinux_elf64_auxv
{
	ulinux_u64 type;	/* entry type */
	ulinux_u64 val;		/* integer value */
};

/******************************************************************************/
#define ULINUX_EI_NIDENT 16

struct ulinux_elf64_ehdr {
	ulinux_u8	ident[ULINUX_EI_NIDENT];/* Magic number and other info */
	ulinux_u16	type;			/* Object file type */
	ulinux_u16	machine;		/* Architecture */
	ulinux_u32	version;		/* Object file version */
	ulinux_u64	entry;			/* Entry point virtual address */
	ulinux_u64	phoff;			/* Program segment header table file offset */
	ulinux_u64	shoff;			/* Section header table file offset */
	ulinux_u32	flags;			/* Processor-specific flags */
	ulinux_u16	ehsize;			/* ELF header size in bytes */
	ulinux_u16	phentsize;		/* Program segment header table entry size */
	ulinux_u16	phnum;			/* Program segment header table entry count */
	ulinux_u16	shentsize;		/* Section header table entry size */
	ulinux_u16	shnum;			/* Section header table entry count */
	ulinux_u16	shstrndx;		/* Section header string table index */
};
/******************************************************************************/


/******************************************************************************/
struct ulinux_elf64_phdr {
	ulinux_u32	type;	/* program segment type */
	ulinux_u32	flags;	/* program segment flags */
	ulinux_u64	offset;	/* program segment file offset */
	ulinux_u64	vaddr;	/* program segment virtual address */
	ulinux_u64	paddr;	/* program segment physical address */
	ulinux_u64	filesz;	/* program segment size in file */
	ulinux_u64	memsz;	/* program segment size in memory */
	ulinux_u64	align;	/* program segment alignment */
};
/*----------------------------------------------------------------------------*/
/* program segment types */
#define	ULINUX_PT_NULL		0		/* Program header table entry unused */
#define ULINUX_PT_LOAD		1		/* Loadable program segment */
#define ULINUX_PT_DYNAMIC	2		/* Dynamic linking information */
#define ULINUX_PT_INTERP	3		/* Program interpreter */
#define ULINUX_PT_NOTE		4		/* Auxiliary information */
#define ULINUX_PT_SHLIB		5		/* Reserved */
#define ULINUX_PT_PHDR		6		/* Entry for header table itself */
#define ULINUX_PT_TLS		7		/* Thread-local storage segment */
#define	ULINUX_PT_NUM		8		/* Number of defined types */
#define ULINUX_PT_LOOS		0x60000000	/* Start of OS-specific */
#define ULINUX_PT_GNU_EH_FRAME	0x6474e550	/* GCC .eh_frame_hdr segment */
#define ULINUX_PT_GNU_STACK	0x6474e551	/* Indicates stack executability */
#define ULINUX_PT_GNU_RELRO	0x6474e552	/* Read-only after relocation */
#define ULINUX_PT_LOSUNW	0x6ffffffa
#define ULINUX_PT_SUNWBSS	0x6ffffffa	/* Sun Specific segment */
#define ULINUX_PT_SUNWSTACK	0x6ffffffb	/* Stack segment */
#define ULINUX_PT_HISUNW	0x6fffffff
#define ULINUX_PT_HIOS		0x6fffffff	/* End of OS-specific */
#define ULINUX_PT_LOPROC	0x70000000	/* Start of processor-specific */
#define ULINUX_PT_HIPROC	0x7fffffff	/* End of processor-specific */
/******************************************************************************/



/******************************************************************************/
struct ulinux_elf64_dyn {
	ulinux_s64 tag;		/* Dynamic entry type */
	union {
		ulinux_u64 val;	/* Integer value */
		ulinux_u64 ptr;	/* Address value */
	};
};
/*============================================================================*/
/* values for tag */
/*----------------------------------------------------------------------------*/
/* major values for tag */
#define ULINUX_DT_NULL			0			/* Marks end of dynamic section */
#define ULINUX_DT_NEEDED		1			/* Name of needed library */
#define ULINUX_DT_PLTRELSZ		2			/* Size in bytes of PLT relocs */
#define ULINUX_DT_PLTGOT		3			/* Processor defined value */
#define ULINUX_DT_HASH			4			/* Address of symbol hash table */
#define ULINUX_DT_STRTAB		5			/* Address of string table */
#define ULINUX_DT_SYMTAB		6			/* Address of symbol table */
#define ULINUX_DT_RELA			7			/* Address of Rela relocs */
#define ULINUX_DT_RELASZ		8			/* Total size of Rela relocs */
#define ULINUX_DT_RELAENT		9			/* Size of one Rela reloc */
#define ULINUX_DT_STRSZ			10			/* Size of string table */
#define ULINUX_DT_SYMENT		11			/* Size of one symbol table entry */
#define ULINUX_DT_INIT			12			/* Address of init function */
#define ULINUX_DT_FINI			13			/* Address of termination function */
#define ULINUX_DT_SONAME		14			/* Name of shared object */
#define ULINUX_DT_RPATH			15			/* Library search path (deprecated) */
#define ULINUX_DT_SYMBOLIC		16			/* Start symbol search here */
#define ULINUX_DT_REL			17			/* Address of Rel relocs */
#define ULINUX_DT_RELSZ			18			/* Total size of Rel relocs */
#define ULINUX_DT_RELENT		19			/* Size of one Rel reloc */
#define ULINUX_DT_PLTREL		20			/* Type of reloc in PLT */
#define ULINUX_DT_DEBUG			21			/* For debugging; unspecified */
#define ULINUX_DT_TEXTREL		22			/* Reloc might modify .text */
#define ULINUX_DT_JMPREL		23			/* Address of PLT relocs */
#define	ULINUX_DT_BIND_NOW		24			/* Process relocations of object */
#define	ULINUX_DT_INIT_ARRAY		25			/* Array with addresses of init fct */
#define	ULINUX_DT_FINI_ARRAY		26			/* Array with addresses of fini fct */
#define	ULINUX_DT_INIT_ARRAYSZ		27			/* Size in bytes of DT_INIT_ARRAY */
#define	ULINUX_DT_FINI_ARRAYSZ		28			/* Size in bytes of DT_FINI_ARRAY */
#define ULINUX_DT_RUNPATH		29			/* Library search path */
#define ULINUX_DT_FLAGS			30			/* Flags for the object being loaded */
#define ULINUX_DT_ENCODING		32			/* Start of encoded range */
#define ULINUX_DT_PREINIT_ARRAY 	32			/* Array with addresses of preinit fct*/
#define ULINUX_DT_PREINIT_ARRAYSZ	33			/* size in bytes of DT_PREINIT_ARRAY */
#define	ULINUX_DT_NUM			34			/* Number used */
#define ULINUX_DT_LOOS			0x6000000d		/* Start of OS-specific */
#define ULINUX_DT_HIOS			0x6ffff000		/* End of OS-specific */
/* here, there are many more tags, see below */
#define ULINUX_DT_LOPROC		0x70000000		/* Start of processor-specific */
#define ULINUX_DT_HIPROC		0x7fffffff		/* End of processor-specific */
#define	ULINUX_DT_PROCNUM		ULINUX_DT_MIPS_NUM	/* Most used by any processor */
/*----------------------------------------------------------------------------*/
/* the versioning entry types: both extensions may be used at the same time */
/*  GNU extension  */
#define ULINUX_DT_VERSYM		0x6ffffff0

#define ULINUX_DT_RELACOUNT		0x6ffffff9
#define ULINUX_DT_RELCOUNT		0x6ffffffa

/* Sun(now oracle) extension */
#define ULINUX_DT_FLAGS_1		0x6ffffffb			/* State flags, see DF_1_* below.  */
#define	ULINUX_DT_VERDEF		0x6ffffffc			/* Address of version definition table */
#define	ULINUX_DT_VERDEFNUM		0x6ffffffd			/* Number of version definitions */
#define	ULINUX_DT_VERNEED		0x6ffffffe			/* Address of table with needed versions */
#define	ULINUX_DT_VERNEEDNUM		0x6fffffff			/* Number of needed versions */
#define ULINUX_DT_VERSIONTAGIDX(tag)	(ULINUX_DT_VERNEEDNUM - (tag))	/* Reverse order! */
#define ULINUX_DT_VERSIONTAGNUM		16
/*----------------------------------------------------------------------------*/
/* values for tag */
/*============================================================================*/
/******************************************************************************/


/******************************************************************************/
/* 
 * here, only dynamic symbols, we are not a compiler linker, we are
 * in the dynamic use case
 */
struct ulinux_elf64_sym {
	ulinux_u32	name;	/* Symbol name (string tbl index) */
	/* Symbol type and binding: 0xf0 is bind, 0x0f is type */
	ulinux_u8	info;	
	ulinux_u8	other;	/* Symbol visibility */
	ulinux_u16	shndx;	/* Section index */
	ulinux_u64	value;	/* Symbol value */
	ulinux_u64	size;	/* Symbol size */
};
/*----------------------------------------------------------------------------*/
/* Legal values for ST_BIND subfield of st_info (symbol binding).  */
#define ULINUX_STB_LOCAL	0	/* Local symbol */
#define ULINUX_STB_GLOBAL	1	/* Global symbol */
#define ULINUX_STB_WEAK		2	/* Weak symbol */
#define	ULINUX_STB_NUM		3	/* Number of defined types.  */
#define ULINUX_STB_LOOS		10	/* Start of OS-specific */
#define ULINUX_STB_GNU_UNIQUE	10	/* Unique symbol.  */
#define ULINUX_STB_HIOS		12	/* End of OS-specific */
#define ULINUX_STB_LOPROC	13	/* Start of processor-specific */
#define ULINUX_STB_HIPROC	15	/* End of processor-specific */
/*----------------------------------------------------------------------------*/
/* Legal values for ST_TYPE subfield of st_info (symbol type).  */
#define ULINUX_STT_NOTYPE	0	/* Symbol type is unspecified */
#define ULINUX_STT_OBJECT	1	/* Symbol is a data object */
#define ULINUX_STT_FUNC		2	/* Symbol is a code object */
#define ULINUX_STT_SECTION	3	/* Symbol associated with a section */
#define ULINUX_STT_FILE		4	/* Symbol's name is file name */
#define ULINUX_STT_COMMON	5	/* Symbol is a common data object */
#define ULINUX_STT_TLS		6	/* Symbol is thread-local data object*/
#define	ULINUX_STT_NUM		7	/* Number of defined types.  */
#define ULINUX_STT_LOOS		10	/* Start of OS-specific */
#define ULINUX_STT_GNU_IFUNC	10	/* Symbol is indirect code object */
#define ULINUX_STT_HIOS		12	/* End of OS-specific */
#define ULINUX_STT_LOPROC	13	/* Start of processor-specific */
#define ULINUX_STT_HIPROC	15	/* End of processor-specific */
/*----------------------------------------------------------------------------*/
/*
 * Symbol table indices are found in the hash buckets and chain table
 * of a symbol hash table section. This special index value indicates
 * the end of a chain, meaning no further symbols are found in that bucket.
 */
#define ULINUX_STN_UNDEF	0		/* End of a chain.  */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
#endif
