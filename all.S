/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#define __ASSEMBLY__
#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
#include "config.h"

#include "ulinux/arch/sysc.S"
#include "ulinux/arch/start.S"
#include "ulinux/arch/utils/endian.S"
#include "ulinux/arch/utils/mem.S"
#undef __ASSEMBLY__
