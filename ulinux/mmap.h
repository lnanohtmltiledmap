#ifndef ULINUX_MMAP_H
#define ULINUX_MMAP_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#include <ulinux/arch/mmap.h>

#define ULINUX_MAP_GROWSDOWN	0x0100	/* stack-like segment */
#define ULINUX_MAP_DENYWRITE	0x0800	/* ETXTBSY */
#define ULINUX_MAP_EXECUTABLE	0x1000	/* mark it as an executable */
#define ULINUX_MAP_LOCKED	0x2000	/* pages are locked */
#define ULINUX_MAP_NORESERVE	0x4000	/* don't check for reservations */
#define ULINUX_MAP_POPULATE	0x8000	/* populate (prefault) pagetables */
#define ULINUX_MAP_NONBLOCK	0x10000	/* do not block on IO */
#define ULINUX_MAP_STACK	0x20000	/*
					 * give out an address that is best
					 * suited for process/thread stacks
					 * XXX:it's a NOOP, linux does not use
					 * it
					 */
#define ULINUX_MAP_HUGETLB	0x40000	/* create a huge page mapping */
#define ULINUX_MAP_SYNC		0x80000	/*
					 * perform synchronous page faults for
					 * the mapping
					 */

#define ULINUX_MCL_CURRENT	1	/* lock all current mappings */
#define ULINUX_MCL_FUTURE	2	/* lock all future mappings */
#define ULINUX_MCL_ONFAULT	4	/* lock all pages that are faulted in */

/*---------------------------------------------------------------------------*/

#define ULINUX_PROT_READ	0x1		/* page can be read */
#define ULINUX_PROT_WRITE	0x2		/* page can be written */
#define ULINUX_PROT_EXEC	0x4		/* page can be executed */
#define ULINUX_PROT_SEM		0x8		/*
						 * page may be used for atomic
						 * ops
						 */
#define ULINUX_PROT_NONE	0x0		/* page can not be accessed */
#define ULINUX_PROT_GROWSDOWN	0x01000000	/*
						 * mprotect flag: extend change
						 * to start of growsdown vma
						 */
#define ULINUX_PROT_GROWSUP	0x02000000	/*
						 * mprotect flag: extend change
						 * to end of growsup vma
						 */

#define ULINUX_MAP_TYPE			0x0f	/* Mask for type of mapping */
#define ULINUX_MAP_FIXED		0x10	/* Interpret addr exactly */
#define ULINUX_MAP_ANONYMOUS		0x20	/* don't use a file */
#define ULINUX_MAP_UNINITIALIZED	0x4000000 

/*---------------------------------------------------------------------------*/

#define ULINUX_MAP_SHARED	0x01	/* Share changes */
#define ULINUX_MAP_PRIVATE	0x02	/* Changes are private */

#define ULINUX_MREMAP_MAYMOVE	0x1
#define ULINUX_MREMAP_FIXED	0x2
#endif
