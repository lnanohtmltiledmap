#ifndef ULINUX_UTILS_QSORT_MACROS_H
#define ULINUX_UTILS_QSORT_MACROS_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#define ULINUX_QSORT_CMP_LT -1
#define ULINUX_QSORT_CMP_EQ 0
#define ULINUX_QSORT_CMP_GT 1
#endif /* ULINUX_UTILS_QSORT_MACROS_H */
/******************************************************************************/
/******************************************************************************/
/* protection for inclusion in qsort.c */
#ifndef ULINUX_UTILS_QSORT_C
#ifndef ULINUX_UTILS_QSORT_H
#define ULINUX_UTILS_QSORT_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT extern
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/
ULINUX_EXPORT void ulinux_qsort(void *base, ulinux_u64 n, ulinux_u64 sz,
					ulinux_s8 (*cmp)(void *a, void *b));
/*----------------------------------------------------------------------------*/
#undef ULINUX_EXPORT
#endif /* ULINUX_UTILS_QSORT_C */
#endif /* ULINUX_UTILS_QSORT_H */
