#ifndef ULINUX_UTILS_MEM_H
#define ULINUX_UTILS_MEM_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define EXPORT extern
#else
#define EXPORT static
#endif
/*----------------------------------------------------------------------------*/
#define u8 ulinux_u8
#define u64 ulinux_u64
#define memcpy_forward ulinux_memcpy_forward
#define memset ulinux_memset
#define memcmp ulinux_memcmp
/*----------------------------------------------------------------------------*/
EXPORT void memcpy_forward(void *d, void *s, u64 len);
EXPORT void memset(void *d, u8 c, u64 len);
EXPORT bool memcmp(void *s1, void *s2, u64 len);
/*----------------------------------------------------------------------------*/
#undef u8
#undef u64
#define memcpy_forward ulinux_memcpy_forward
#define memset ulinux_memset
#define memcmp ulinux_memcmp
/*----------------------------------------------------------------------------*/
#undef EXPORT
#endif
