#ifndef ULINUX_ARCHS_AARCH64_UTILS_ENDIAN_S
#define ULINUX_ARCHS_AARCH64_UTILS_ENDIAN_S
#ifdef ULINUX_ASSEMBLY_ENDIAN
#ifdef __GNUAS__
.arch armv8-a
.text
.global ulinux_cpu_to_be16
.type ulinux_cpu_to_be16, STT_FUNC
.hidden ulinux_cpu_to_be16
.align 2
ulinux_cpu_to_be16:
	rev16 w0, w0
	ret
.global ulinux_cpu_to_be32
.type ulinux_cpu_to_be32, STT_FUNC
.hidden ulinux_cpu_to_be32
.align 2
ulinux_cpu_to_be32:
	rev32 x0, x0
	ret
.global ulinux_cpu_to_be64
.type ulinux_cpu_to_be64, STT_FUNC
.hidden ulinux_cpu_to_be64
.align 2
ulinux_cpu_to_be64:
	rev x0, x0
	ret
#else
#error "your assembler is not supported with tinycc for endian handling"
#endif /* __GNUAS__ */
#endif /* ULINUX_ASSEMBLY_ENDIAN */
#endif
