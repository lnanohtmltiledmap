#ifndef ULINUX_UTILS_ASCII_BLOCK_CONV_DECIMAL_DECIMAL_H
#define ULINUX_UTILS_ASCII_BLOCK_CONV_DECIMAL_DECIMAL_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#include <stdbool.h>
/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define EXPORT extern
#else
#define EXPORT static
#endif
/*----------------------------------------------------------------------------*/
#define u8 ulinux_u8
#define u16 ulinux_u16
#define u32 ulinux_u32
#define u64 ulinux_u64
#define dec2u8_blk  ulinux_dec2u8_blk
#define dec2u16_blk ulinux_dec2u16_blk
#define dec2u32_blk ulinux_dec2u32_blk
#define dec2u64_blk ulinux_dec2u64_blk
/*----------------------------------------------------------------------------*/
EXPORT bool dec2u8_blk(u8 *dest, u8 *start, u8 *last);
EXPORT bool dec2u16_blk(u16 *dest, u8 *start, u8 *last);
EXPORT bool dec2u32_blk(u32 *dest, u8 *start, u8 *last);
EXPORT bool dec2u64_blk(u64 *dest, u8 *start, u8 *last);
/*----------------------------------------------------------------------------*/
#undef u8
#undef u16
#undef u32
#undef u64
#undef dec2u8_blk
#undef dec2u16_blk
#undef dec2u32_blk
#undef dec2u64_blk
/*----------------------------------------------------------------------------*/
#undef ULINUX_EXPORT
#endif
