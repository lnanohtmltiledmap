#ifndef LNANOHTMLTILEDMAP_HTTP_C
#define LNANOHTMLTILEDMAP_HTTP_C
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#include <stdbool.h>
#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
#include <ulinux/start.h>
#include <ulinux/error.h>
#include <ulinux/sysc.h>
#include <ulinux/file.h>
#include <ulinux/time.h>
#include <ulinux/mmap.h>
#include <ulinux/signal/signal.h>
#include <ulinux/socket/socket.h>
#ifdef CONFIG_IPV4
#include <ulinux/socket/in.h>
#else
#include <ulinux/socket/in6.h>
#endif
#include <ulinux/epoll.h>
#include <ulinux/utils/endian.h>

#include "config.h"
#include "common_cpp.h"
#include "html.h"
#include "exit_codes.h"
/*============================================================================*/
#include "namespace/ulinux.h"
/*============================================================================*/
#define SIGBIT(sig) (1<<(sig-1))
#ifdef CONFIG_IPV4
static struct sockaddr_in srv_addr;
#else
static struct sockaddr_in6 srv_addr;
#endif
static sl page_sz;	/* from linux auxiliary vector */
static si srv_sock;	/* the main listening socket */
static si sigs_fd;	/* synchronous signal handling */
static si epfd;		/* the main epoll file descriptor */
static si cnx_sock;	/* a connection socket from accept */
static si idletimerfd;	/* the idle timer for the connection */
static u64 urandom;	/* to try to workaround client cache */
/* request line */
#define RFC7230_REQ_LINE_RECOMMENDED_MIN_SZ 8000
static u8 *req;		/* buffer for the request line buffer */
static u8 *req_e;	/* pointer on the byte past the last mmaped byte */
static u8 *req_recv;	/* pointer on the byte past the last received byte */
static u32 req_sz_max;	/* the size of buffer for the request line */

/* state machine */
#define STATE_RDY			0x00
#define STATE_RECV_REQ_LINE 		0x01
#define STATE_DISCARD_REQ_REM		0x02
#define STATE_SEND_RESP			0x03
static u8 state;
/* data storage for states */
static u8 *req_line_crlf_lookup; /* tracker pointer for the request line CRLF */
static u8 *req_line_e; /* pointer on the '\r' of the request line CRLF */
/*
 * count how much of the crlf crlf sequence we have seen in order to locate
 * the end of a request without a body. If we get a weird request with a
 * huge payload, the idle timeout will get rid of the connection
 */
static u8 req_crlf_crlf_lookup;
static u8 *crlf_crlf;
static u8 *aux;	/* auxiliary buffer of page_sz bytes */
static u8 *aux_e;/* pointer on the byte past the last mmaped byte */
/* resp */
static u8 *resp;
static u8 *resp_p;	/* track what we have sent already */
static u8 *resp_html_e; /* pointer past the last byte of the generated response */
static u8 *resp_e;
static u32 resp_sz_max;
/*----------------------------------------------------------------------------*/
#define AT_NULL		0
#define AT_PAGESIZE	6
struct auxv_t {
	sl type;
	sl data;
};
static void pagesize_get(u8 *abi_stack)
{
	abi_stack += sizeof(ul); /* skip argc */
	/* argv */
	loop {
		ul *p;

		p = (ul*)abi_stack;
		if (*p == 0)
			break;
		abi_stack += sizeof(ul);
	}
	abi_stack += sizeof(ul); /* skip argv terminator */
	loop {
		ul *p;

		p = (ul*)abi_stack;
		if (*p == 0)
			break;
		abi_stack += sizeof(ul);
	}
	abi_stack += sizeof(ul); /* skip envp terminator */
	loop {
		struct auxv_t *auxv;

		auxv = (struct auxv_t*)abi_stack;
		if (auxv->type == AT_NULL)
			exit(SETUP_AUXVEC_PAGESIZE_NOT_FOUND);
		else if (auxv->type == AT_PAGESIZE) {
			page_sz = (u32)auxv->data;
			break;
		}
		abi_stack += 2 * sizeof(ul);
	}
}
#undef AT_NULL
#undef AT_PAGESIZE
/* best effort */
static void urandom_init(void)
{
	si fd;

	loop {
		sl r;
		r = openat(0, "/dev/urandom", O_RDONLY, 0);
		if (r != -EINTR) {
			if (ISERR(r)) /* ignored */
				return;
			fd = (si)r;
			break;
		}
	}
	read(fd, &urandom, sizeof(urandom));
	close(fd);
}
static void globals_init(u8 *abi_stack)
{
	/* XXX: sockaddr_in? structures are of different sizes */
	memset(&srv_addr, 0, sizeof(srv_addr));
#ifdef CONFIG_IPV4
	srv_addr.family = AF_INET;
	/* big endian port */
	srv_addr.port = cpu_to_be16_const(CONFIG_LISTENING_PORT);
	/* zero address = any ipv4 address */
#else
	srv_addr.family = AF_INET6;
	/* big endian port */
	srv_addr.port = cpu_to_be16_const(CONFIG_LISTENING_PORT);
	/* zero address = any ipv6 address */
#endif
	srv_sock = -1;
	cnx_sock = -1;
	sigs_fd = -1;
	epfd = -1;
	cnx_sock = -1;
	crlf_crlf = "\r\n\r\n";
	state = STATE_RDY;
	pagesize_get(abi_stack);
	urandom_init();
}
/*
 * handling of synchronous signals with signalfd
 * cannot change SIGKILL, neither SIGSTOP
 */
static void sigs_setup(void)
{
	u64 mask;
	sl r;

	mask = ~0;
	r = rt_sigprocmask(SIG_BLOCK, &mask, 0, sizeof(mask));
	if (ISERR(r))
		exit(SIGS_SETUP_BLOCKING_FAILURE);
	mask = SIGBIT(SIGTERM);
	sigs_fd = (si)signalfd4(-1, &mask, sizeof(mask), SFD_NONBLOCK);
	if (ISERR(sigs_fd))
		exit(SIGS_SETUP_HANDLERS_FAILURE);
}
static void epoll_sigs_setup(void)
{
	struct epoll_event ep_evt;
	sl r;

	memset(&ep_evt, 0, sizeof(ep_evt));
	ep_evt.events = EPOLLET | EPOLLIN;
	ep_evt.data.fd = sigs_fd;
	r = epoll_ctl(epfd, EPOLL_CTL_ADD, sigs_fd, &ep_evt);
	if (ISERR(r))
		exit(EPOLL_SIGS_SETUP_EPOLL_ADD_FAILURE);
}
static void srv_sock_create(void)
{
	sl bool_true;
	sl r;
#ifdef CONFIG_IPV4
	r = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC, 0);
#else
	r = socket(AF_INET6, SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC, 0);
#endif
	if (ISERR(r))
		exit(SRV_SOCK_CREATE_FAILURE);
	srv_sock = (si)r;
	bool_true = 1;
	r = setsockopt(srv_sock, SOL_SOCKET, SO_REUSEADDR, &bool_true,
							sizeof(bool_true));
	if (ISERR(r))
		exit(SRV_SOCK_SET_SOCK_OPTION_FAILURE);
	r = bind(srv_sock, &srv_addr, sizeof(srv_addr));
	if (ISERR(r))
		exit(SRV_SOCK_BIND_FAILURE);
	r = listen(srv_sock, 0);
	if (ISERR(r))
		exit(SRV_SOCK_LISTEN_FAILURE);
}
static void epoll_srv_sock_setup(void)
{
	struct epoll_event ep_evt;
	sl r;

	memset(&ep_evt, 0, sizeof(ep_evt));
	ep_evt.events = EPOLLIN | EPOLLPRI;
	ep_evt.data.fd = srv_sock;
	r = epoll_ctl(epfd, EPOLL_CTL_ADD, srv_sock, &ep_evt);
	if(ISERR(r))
		exit(SRV_SOCK_SETUP_EPOLL_CTL_ADD_FAILURE);
}
static void req_mmap(void)
{
	sl addr;

	req_sz_max = RFC7230_REQ_LINE_RECOMMENDED_MIN_SZ / page_sz + page_sz;
	addr = mmap(req_sz_max, RD | WR, PRIVATE | ANONYMOUS);
	if(addr == 0 || ISERR(addr))
		exit(PAGE_MMAP_FAILURE);
	req = (u8*)addr;
	req_e = req + req_sz_max;
	req_recv = req;
	/* random initial content */
}
static void aux_mmap(void)
{
	sl addr;

	addr = mmap(page_sz, RD | WR, PRIVATE | ANONYMOUS);
	if(addr == 0 || ISERR(addr))
		exit(PAGE_MMAP_FAILURE);
	aux = (u8*)addr;
	aux_e = req + page_sz;
	/* random initial content */
}
static void resp_mmap(void)
{
	sl addr;

	/* enough room for our template, don't need much more */
	resp_sz_max = html_sz_max();
	resp_sz_max = resp_sz_max / page_sz + page_sz;
	addr = mmap(resp_sz_max, RD | WR, PRIVATE | ANONYMOUS);
	if(addr == 0 || ISERR(addr))
		exit(PAGE_MMAP_FAILURE);
	resp = (u8*)addr;
	resp_e = resp + resp_sz_max;
	/* random initial content */
}
static void idletimerfd_create(void)
{
	sl r;

	r = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK | TFD_CLOEXEC);
	if (ISERR(r))
		exit(IDLETIMERFD_UNABLE_TO_CREATE_FD);
	idletimerfd = (si)r;
}
static void epoll_idletimerfd_setup(void)
{
	struct epoll_event ep_evt;
	sl r;

	memset(&ep_evt, 0, sizeof(ep_evt));
	ep_evt.events = EPOLLIN;
	ep_evt.data.fd = idletimerfd;
	r = epoll_ctl(epfd, EPOLL_CTL_ADD, idletimerfd, &ep_evt);
	if (ISERR(r))
		exit(EPOLL_IDLETIMERFD_SETUP_EPOLL_ADD_FAILURE);
}
static void setup(void)
{
	sigs_setup();
	epfd =(si)epoll_create1(0);
	if (ISERR(epfd))
		exit(SETUP_EPOLL_CREATE_FAILURE);
	epoll_sigs_setup();
	srv_sock_create();
	epoll_srv_sock_setup();
	idletimerfd_create();
	epoll_idletimerfd_setup();
	req_mmap();
	aux_mmap();
	resp_mmap();
}
/* consuming synchronous signals */
static void sigs_consume(void)
{
	struct signalfd_siginfo info;
	loop {
		sl r;

		loop {
			memset(&info, 0, sizeof(info));
			/* atomic read / no short reads last time we checked */
			r = read(sigs_fd, &info, sizeof(info));
			if (r != -EINTR)
				break;
		}
		/* not supposed to happen, but we do it anyway */
		if (r != -EAGAIN && ((ISERR(r) || (r > 0
							&& r != sizeof(info)))))
			exit(SIGS_CONSUME_SIGINFO_READ_FAILURE);
		if (r == 0 || r == -EAGAIN)
			break;
		switch (info.ssi_signo) {
		case SIGTERM:
			exit(0);
			break;
		/* please, do add the ones you like */
		}
	}
}
/* reasonable and generic idle timer, for now */
static void idletimer_arm(void)
{
	sl r;
	struct itimerspec t;

	memset(&t, 0, sizeof(t));
	t.value.sec = CONFIG_IDLETIMER_TIMEOUT_SECONDS;
	r = timerfd_settime(idletimerfd, 0, &t, 0);
	if (ISERR(r))
		exit(IDLETIMERFD_FAILED_TO_ARM);
}
static void idletimer_disarm(void)
{
	sl r;
	struct itimerspec t;

	memset(&t, 0, sizeof(t));
	r = timerfd_settime(idletimerfd, 0, &t, 0);
	if (ISERR(r))
		exit(IDLETIMERFD_FAILED_TO_DISARM);
}
/* may log any pb one day */
static void state_rdy_force(void)
{
	struct epoll_event ep_evt;
	/*
	 * XXX: events can be reported for a "closed" file descriptor: *ALL*
	 * file descriptors related to the underlaying objects must be closed
	 * in order to avoid spurious events
	 */
	epoll_ctl(epfd, EPOLL_CTL_DEL, cnx_sock, 0);
	close(cnx_sock);
	cnx_sock = -1;
	memset(&ep_evt, 0, sizeof(ep_evt));
	/* switch on the listening socket */
	ep_evt.events = EPOLLIN | EPOLLPRI;
	ep_evt.data.fd = srv_sock;
	epoll_ctl(epfd, EPOLL_CTL_MOD, srv_sock, &ep_evt);
	idletimer_disarm();
	state = STATE_RDY;
}
static bool state_recv_req_line(si cnx_sock_from_accept)
{
	struct epoll_event ep_evt;
	sl r;

	/* must be in ready state */
	if (state != STATE_RDY)
		goto force_rdy;
	/* install the connection socket */
	memset(&ep_evt, 0, sizeof(ep_evt));
	ep_evt.events = EPOLLIN | EPOLLPRI; /* EPOLLPRI = TCP urgent data */
	ep_evt.data.fd = cnx_sock_from_accept;
	r = epoll_ctl(epfd, EPOLL_CTL_ADD, cnx_sock_from_accept, &ep_evt);
	if(ISERR(r)) /* since we are from the ready state, no EEXIST */
		goto force_rdy;
	/* ignore events from the listening socket */
	memset(&ep_evt, 0, sizeof(ep_evt));
	ep_evt.data.fd = srv_sock;
	r = epoll_ctl(epfd, EPOLL_CTL_MOD, srv_sock, &ep_evt);
	if(ISERR(r))
		goto force_rdy;
	cnx_sock = cnx_sock_from_accept;
	req_recv = req;
	req_line_crlf_lookup = req;
	idletimer_arm();
	state = STATE_RECV_REQ_LINE;
	return true;
force_rdy:
	state_rdy_force();
	return false;
}
static void state_send_resp(struct html_ctx_t *c)
{
	struct epoll_event ep_evt;

	resp_p = resp;
	resp_html_e = c->p; /* point on the byte past the last generated byte */

	memset(&ep_evt, 0, sizeof(ep_evt));
	ep_evt.events = EPOLLOUT | EPOLLPRI;
	ep_evt.data.fd = cnx_sock;
	epoll_ctl(epfd, EPOLL_CTL_MOD, cnx_sock, &ep_evt);
	idletimer_arm();
	state = STATE_SEND_RESP;
}
/* unforgiving */
static void req_decode(struct html_query_t *q)
{
	u8 *p;
	u8 *q_s;
	u8 *q_e;

	p = req;
	/* skip the http verb */
	loop {
		if (p == req_line_e)
			return;
		if (*p == ' ')
			break;
		++p;
	}
	/* skip the spaces between the verb and the request-target */
	loop {
		if (p == req_line_e)
			return;
		if (*p != ' ')
			break;
		++p;
	}
	/* from here we are in the request-target */
	/*
	 * reach the http scheme URI '?' query delimiter (which should be uniq)
	 * or the various ends
	 */
	loop {
		if (p == req_line_e || *p == ' ')
			return;
		if (*p == '?')
			break;
		++p;
	}
	/* here, we have a html query marker '?' */
	++p; /* skip the marker '?' */
	q_s = p;
	q_e = p;
	loop {
		if (p == req_line_e || *p == '#' || *p == ' ') {
			q_e = p;
			break;
		}
		++p;
	}
	html_query_decode(q, q_s, q_e);
}
/*
 * we use such function because scanning of CRLFCRLF can happen across 2
 * buffers: the request-line buffer and the auxiliary buffer used to discard
 * the remainder of the request
 */
static bool crlf_crlf_scan(u8 *start, u8 *end)
{ loop {
	if (start == end)
		return false;
	if (*start == crlf_crlf[req_crlf_crlf_lookup]) {
		if (req_crlf_crlf_lookup == 3)
			return true;
		++req_crlf_crlf_lookup;
	} else /* not the CRLFCRLF sequence */
		req_crlf_crlf_lookup = 0;
	++start;
}}
static void resp_compute(void)
{
	struct html_query_t q;
	struct html_ctx_t c;

	html_query_init(&q);
	req_decode(&q);

	html_gen_ctx_init_from_query(&c, &q, resp, urandom);
	++urandom;
	html_majortbl_gen(&c);
		
	state_send_resp(&c);
}
static void state_discard_req_rem(u8 *req_line_cr)
{
	/* we will use data from this state */
	if (state != STATE_RECV_REQ_LINE) {
		state_rdy_force();
		return;
	}
	/* we are still reading from the connection socket */
	req_line_e = req_line_cr;
	/* we have seen already the crlf from the request-line */
	req_crlf_crlf_lookup = 2;
	/* scan what we have already received in the request line buffer */
	if (crlf_crlf_scan(req_line_e + 2, req_recv)) {
		/* got the request terminationg CRLFCRLF */
		resp_compute();
		return;
	}
	/* the request terminating CRLFCRLF were not received yet */
	idletimer_arm();
	state = STATE_DISCARD_REQ_REM;
}
/* we may get more or less */
static void state_discard_req_rem_sock_evts(u32 sock_evts)
{ loop {
	sl r;

	r = read(cnx_sock, aux, page_sz);
	if (r == -EINTR)
		continue; /* SIGSTOP? */
	else if (r == -EAGAIN || r == -EWOULDBLOCK)
		break; /* no more available */
	/*
	 * error or the client did shutdown the connection while in the
	 * syscall (r = 0 with page_sz being non zero)
	 */
	else if (ISERR(r) || r == 0) {
		state_rdy_force();
		return;
	}
	/* r > 0 */
	if (crlf_crlf_scan(aux, aux + r)) {
		resp_compute();
		return;
	}
}}
/* we may get more or less */
static void state_recv_req_line_sock_evts(u32 sock_evts)
{
	u8 *p;

	loop { /* read of much as we can */
		sl r;
		ul buf_remaining_bytes_n;

		/* here, we must have req_e != req_recv */
		buf_remaining_bytes_n = (ul)(req_e - req_recv);
		r = read(cnx_sock, req_recv, buf_remaining_bytes_n);
		if (r == -EINTR)
			continue; /* SIGSTOP? */
		else if (r == -EAGAIN || r == -EWOULDBLOCK)
			break; /* no more available, do something with that */
		/*
		 * error or the client did shutdown the connection while in the
		 * syscall
		 */
		else if (ISERR(r) || (buf_remaining_bytes_n != 0 && r == 0)) {
			state_rdy_force();
			return;
		}
		/* r >= 0 */
		req_recv += r;
		if (req_recv == req_e)
			break; /* no more room, try to do something with that */
	}
	/* lookup for the request-line terminating CRLF */
	p = req_line_crlf_lookup;
	loop {
		if (p == req_recv) {
			req_line_crlf_lookup = p;
			break; /* nothing yet */
		}
		if (p[0] == '\n') {
			if (((p - 1) >= req) && (p[-1] == '\r')) {
				state_discard_req_rem(&p[-1]);
				return;
			}
			/* \n without a previous \r, don't like that */ 
			state_rdy_force();
			return;
		}
		++p;
	}
	/* nothing interesting and no more room :(, bail out */
	if (req_e == req_recv)
		state_rdy_force();
}
static void state_send_resp_sock_evts(u32 evts)
{
	sl r;

	loop {
		r = write(cnx_sock, resp_p, resp_html_e - resp_p);
		if (r != -EINTR)
			break;
	}
	if (r == -EAGAIN)
		return;
	if (ISERR(r)) { /* ignore and reset */
		state_rdy_force();
		return;
	}
	/* account for the bytes sent and acked */
	resp_p += r;
	if (resp_p == resp_html_e) /* whole response sent and acked */
		state_rdy_force();
}
static void cnxs_consume(void)
{
	/* we service synchronously 1 connection at a time, for now */
	if (state != STATE_RDY)
		return;
	loop {
		sl r;
#ifdef CONFIG_IPV4
		struct sockaddr_in peer;
#else
		struct sockaddr_in6 peer;
#endif
		sl peer_len;

		peer_len = sizeof(peer);
		loop {
			/*
			 * from the linux man page, already pending network
			 * errors for the was-about-to-be-returned network
			 * socket may be returned by the accept syscall, namely
			 * the was-about-to-be-returned network socket is
			 * trashed. It means, if we get errors related to
			 * TCP/IP from the accept syscall, those have to be
			 * considered like EAGAIN.
			 */
			r = accept4(srv_sock, &peer, &peer_len, SOCK_NONBLOCK
								| SOCK_CLOEXEC);
			if (r != -EINTR)
				break;
			/* SIGSTOP? */
		}
		/*
		 * no clearcut and accurate return errors for this syscall:
		 * we will consider all errors as retry-able like EAGAIN
		 * I guess, reading the code would help to sort out error codes
		 * which are fatal from those which would be retry-able.
		 */
		if (ISERR(r) || peer_len != sizeof(peer))
			break;
		/*
		 * from here, we should have a valid socket file descriptor with
		 * an expected peer address size
		 */
		if (state_recv_req_line((si)r))
			break;
		/*
		 * failed state switch, then back in ready state , next accept
	 	 * syscall
		 */
	}
}
static void cnx_sock_evts(u32 evts)
{
	/* common to all states, TCP urgent data is not expected with EPOLLPRI */
	if ((evts & (EPOLLERR | EPOLLHUP | EPOLLPRI)) != 0) {
		state_rdy_force();
		return;
	}
	/* ofc, will get an interface as a function table if going bigger */
	switch(state) {
	case STATE_RECV_REQ_LINE:
		state_recv_req_line_sock_evts(evts);
		break;
	case STATE_DISCARD_REQ_REM:
		state_discard_req_rem_sock_evts(evts);
		break;
	case STATE_SEND_RESP:
		state_send_resp_sock_evts(evts);
		break;
	case STATE_RDY:
		break;
	default:
		state_rdy_force();
		break;
	}
}
static void state_discard_req_rem_idletimerfd_expiration(void)
{
	/* trash the connection */
	state_rdy_force();
}
static void state_recv_req_line_idletimerfd_expiration(void)
{
	/* trash the connection */
	state_rdy_force();
}
static void state_send_resp_idletimerfd_expiration(void)
{
	/* trash the connection */
	state_rdy_force();
}
/*
 * XXX: must read the idletimer file descriptor count of expirations in order to
 * disable EPOLLIN
 */
static void idletimerfd_evts(u32 evts)
{
	u64 expirations_n;

	if ((evts & EPOLLIN) == 0)
		return; /* spurious, may log */
	/* from here EPOLLIN */
	loop {
		sl r;

		/* atomic read */
		expirations_n = 0;
		r = read(idletimerfd, &expirations_n, sizeof(expirations_n));
		if (r == -EINTR)
			continue; /* SIGSTOP? */
		/* we are not supposed to get that in EPOLLIN, may log */
		if (r == -EAGAIN)
			return;
		if (ISERR(r) || r != sizeof(expirations_n))
			exit(IDLETIMERFD_READ_FAILURE);
		/* r == 8  */
		break;
	}
	if (expirations_n == 0) /* spurious, may log */
		return;
	/* ofc, will get an interface as a function table if going bigger */
	switch(state) {
	case STATE_DISCARD_REQ_REM:
		state_discard_req_rem_idletimerfd_expiration();
		break;
	case STATE_RECV_REQ_LINE:
		state_recv_req_line_idletimerfd_expiration();
		break;
	case STATE_SEND_RESP:
		state_send_resp_idletimerfd_expiration();
		break;
	case STATE_RDY:
	default:
		/* never mind */
		break;
	}
}
static void main_loop(void)
{
	loop {
		struct epoll_event evts[2];	/* sigs_fd and srv_sock */
		sl r;
		sl j;
	
		loop {
			memset(evts, 0, sizeof(evts));
			r = epoll_pwait(epfd, evts, 2, -1, 0);
			if (r != -EINTR) /* SIGSTOP? */
				break; 
		}
		if (ISERR(r))
			exit(MAIN_LOOP_EPOLL_WAIT_GENERIC_FAILURE);
		/* r >= 0 from here */
		j = 0;
		loop {
			if (j == r)
				break;
			if (evts[j].data.fd == sigs_fd) {
			  if((evts[j].events & EPOLLIN) != 0)
				sigs_consume();
			  else
				exit(MAIN_LOOP_EPOLL_WAIT_SIGS_FD_EVENT_IS_NOT_EPOLLIN);
			} else if (evts[j].data.fd == srv_sock) {
				if ((evts[j].events & (EPOLLERR | EPOLLHUP
							| EPOLLPRI)) != 0)
					exit(MAIN_LOOP_EPOLL_WAIT_SRV_SOCK_UNEXPECTED_EVENT);
				else if ((evts[j].events & EPOLLIN) != 0) {
					cnxs_consume();
				} else
					exit(MAIN_LOOP_EPOLL_WAIT_SRV_SOCK_UNKNOWN_FAILURE);
			} else if (evts[j].data.fd == cnx_sock)
				cnx_sock_evts(evts[j].events);
			else if (evts[j].data.fd == idletimerfd)
				idletimerfd_evts(evts[j].events);
			++j;
		}
	}
}
/*
 * daemon-ization, not done, maybe one day, in the meantime, usage of
 * setsuid/env/etc commands is recommended
 */
void ulinux_start(u8 *abi_stack)
{
	close(0);
	close(1);
	close(2);
	globals_init(abi_stack);
	setup();
	main_loop();
}
#undef RFC7230_REQ_LINE_RECOMMENDED_MIN_SZ
#undef SIGBIT
#undef STATE_RDY
#undef STATE_RECV_REQ_LINE
#undef STATE_DISCARD_REQ_REM
#define CLEANUP
#include "common_cpp.h"
/*============================================================================*/
#include "namespace/ulinux.h"
/*============================================================================*/
#undef CLEANUP
#endif
