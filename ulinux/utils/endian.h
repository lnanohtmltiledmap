#ifndef ULINUX_UTILS_ENDIAN_H
#define ULINUX_UTILS_ENDIAN_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
/*
 * allow constant basic arithmetic expressions to be passed to the compiler,
 * VERY IMPORTANT !!!
 */
#define ulinux_cpu_to_be16_const(c) (((c & 0xff00) >> 8) | ((c & 0xff) << 8))
#define ulinux_b16_to_cpu_const ulinux_cpu_to_be16_const

#define ulinux_cpu_to_be32_const(c) \
(((c & 0xff000000) >> 24) \
| ((c & 0xff0000) >> 8) \
| ((c & 0xff00) << 8) \
| ((c & 0xff) << 24))
#define ulinux_be32_to_cpu_const ulinux_cpu_to_be32_cpu_const

#define ulinux_cpu_to_be64_const(c) \
(((c & 0xff00000000000000) >> 48) \
| ((c & 0xff000000000000) >> 32) \
| ((c & 0xff0000000000) >> 16) \
| ((c & 0xff00000000) >> 8) \
| ((c & 0xff000000) << 8) \
| ((c & 0xff0000) << 16) \
| ((c & 0xff00) << 32) \
| ((c & 0xff) << 48))
#define ulinux_be64_to_cpu_const ulinux_cpu_to_be64_const
/*----------------------------------------------------------------------------*/
#include <ulinux/arch/utils/endian.h>
#endif
