#ifndef ULINUX_TYPES_POSIX_H
#define ULINUX_TYPES_POSIX_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */

#define uint8_t ulinux_u8
#define uint16_t ulinux_u16
#define uint32_t ulinux_u32
#define uint64_t ulinux_u64
#endif
