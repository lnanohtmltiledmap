#ifndef ULINUX_UTILS_QSORT_C
#define ULINUX_UTILS_QSORT_C
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#include <stdbool.h>

#include <ulinux/compiler_types.h>
#include <ulinux/types.h>

#include <ulinux/utils/qsort.h>
/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/
#define loop for(;;)
#define swap ulinux_qsort_swap
/*----------------------------------------------------------------------------*/
static void swap(void *a, void *b,  ulinux_u64 sz)
{
	ulinux_u8 *a_byte;
	ulinux_u8 *b_byte;

	a_byte = a;
	b_byte = b;
	loop {
		ulinux_u8 tmp;

		if (sz == 0)
			break;

		tmp = *a_byte;
		*a_byte = *b_byte;
		*b_byte = tmp;

		++a_byte;
		++b_byte;
		sz--;
	}
}

#define LT ULINUX_QSORT_CMP_LT
#define LT_PIVOT ULINUX_QSORT_CMP_LT
#define EQ_PIVOT ULINUX_QSORT_CMP_EQ
#define GT_PIVOT ULINUX_QSORT_CMP_GT
ULINUX_EXPORT void ulinux_qsort(void *base, ulinux_u64 n, ulinux_u64 sz,
					ulinux_s8 (*cmp)(void *a, void *b))
{
	ulinux_u8 *left;
	ulinux_u8 *right;
	ulinux_u8 *pivot;
	ulinux_u8 *last;

	/* fast paths for the low values of n, 0 1 and 2 */
	if (n < 2) 
		return;
	else if (n == 2) {
		if (cmp(base + sz, base) == LT)
			swap(base, base + sz, sz);
		return;
	}

	/* from here n >= 3 */

	last = base + (n - 1) * sz;
	left = base;
	right = last;

	/*
	 * setup a pivot which is the best-of-three median with the value from
	 * the middle. Do pre-partition the values from those 3 array slots.
	 * This setup natural bounds.
	 */
	pivot = base + ((n - 1) / 2) * sz;

	if (cmp(pivot, left) == LT_PIVOT)
		swap(left, pivot, sz);
	if (cmp(right, pivot) == LT_PIVOT) {
		swap(pivot, right, sz);
		if (cmp(pivot, left) == LT_PIVOT)
			swap(left, pivot, sz);
	}

	/* the best-of-three median is a fast path for n = 3 */
	if (n == 3)
		return;

	/* from here n >= 4 */

	/*-------------------------------------------------------------------*/
	/*
	 * do partition: 
	 *  - left side LT/EQ the pivot value.
	 *  - right side EQ/GT the pivot value.
	 * both sides can contain slots with the pivot value.
	 */
	left += sz; /* skip the pre-partitioned array left-most value */
	right -= sz; /* skip the pre-partitioned array righ-most value */
	loop {
		ulinux_s8 left_cmp;
		ulinux_s8 right_cmp;

		/*
		 * this loop cannot go forever, the pre-partitioned right most
		 * slot contains a value higher or equal to the pivot
		 * value, and will not change.
		 */
		loop {
			left_cmp = cmp(left, pivot);
			if (left_cmp != LT_PIVOT)
				break;
			left += sz;
		}

		/* same thing than above but backward */
		loop {
			right_cmp = cmp(pivot, right);
			if (right_cmp != LT_PIVOT)
				break;
			right -= sz;
		}

		/*
		 * left points on a slot which contains:
		 *  - the pivot value.
		 *  - a value higher than the pivot value.
		 * all slots below left contains values
		 * lesser than or equal to the pivot value.
		 * 
		 * right points on a slot which contains:
		 *  - the pivot value.
		 *  - a value lower than the pivot value.
		 * all slots above right contains values
		 * higher than or equal to the pivot value.
		 */

		if (right <= left)
			break;

		/* left < right */

		if ((left_cmp != EQ_PIVOT) || (right_cmp != EQ_PIVOT)) {
			swap(left, right, sz);

			/*
			 * we must keep track of the pivot value if it is
			 * swapped
			 */
			if (left == pivot)
				pivot = right;
			else if (right == pivot)
				pivot = left;
		}

		left += sz;
		right -= sz;
	}

	/*
	 * if right < left rewind one slot for each which will be our
	 * boundaries of the partitions. if left == right, then rewind only
	 * left.
	 */
	left -= sz; 
	if (right < left)
		right += sz;
	/*-------------------------------------------------------------------*/

	ulinux_qsort(base, (left - (ulinux_u8*)base) / sz + 1, sz, cmp);
	ulinux_qsort(right, (last - right) / sz + 1, sz, cmp);
}
#undef LT
#undef LT_PIVOT
#undef EQ_PIVOT
#undef GT_PIVOT
/*----------------------------------------------------------------------------*/
#undef loop
#undef swap
#endif
