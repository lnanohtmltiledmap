#ifndef CONFIG_H
#define CONFIG_H
/* will pollute the global namespace */
#ifndef __ASSEMBLY__
#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
/*============================================================================*/
#include "namespace/ulinux.h"
/*============================================================================*/
#endif /* __ASSEMBLY__ */
/* IPV4: without this, the code paths do default to IPv6 */
#define CONFIG_IPV4 1
/*
 * 16 bits value for the port (below 1024, must be root, that you must be for
 * chroot anyway)
 */
#define CONFIG_LISTENING_PORT 41293
#define CONFIG_IDLETIMER_TIMEOUT_SECONDS 4 
/* zoom/x/y.png */
#define CONFIG_TILE_SVR "https://%c.tile.openstreetmap.org/%u/%u/%u.png"
#define CONFIG_TILE_PIXS_N_MAX 256.0
#ifndef __ASSEMBLY__
static u8 config_row_svrs[3] = {'a','b','c'}; /* 1 dns per row to share the load */
#endif
/* X='a'|'b'|'c', u32 for z x and y */
#define CONFIG_TILE_SVR_MAX "https://X.tile.openstreetmap.org/4294967295/4294967295/4294967295.png"
#define CONFIG_HORIZS_N_MAX 3
#define CONFIG_VERTS_N_MAX 3
#ifndef __ASSEMBLY__
/*============================================================================*/
#define CLEANUP
#include "namespace/ulinux.h"
#undef CLEANUP
/*============================================================================*/
#endif /* __ASSEMBLY__ */
#endif
