#ifndef ULINUX_UTILS_ASCII_BLOCK_CONV_HEXADECIMAL_HEXADECIMAL_H
#define ULINUX_UTILS_ASCII_BLOCK_CONV_HEXADECIMAL_HEXADECIMAL_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */

/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT extern 
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/

ULINUX_EXPORT bool ulinux_hex_to_u16_blk(ulinux_u16 *dest, ulinux_u8 *start,
							ulinux_u8 *last);
#undef ULINUX_EXPORT
#endif

