#ifndef ULINUX_UTILS_ASCII_BLOCK_CONV_BINARY_BINARY_C
#define ULINUX_UTILS_ASCII_BLOCK_CONV_BINARY_BINARY_C
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#include <stdbool.h>

#include <ulinux/compiler_types.h>
#include <ulinux/types.h>

/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* local */
#define loop for(;;)
#define CSTRLEN(cstr) (sizeof(cstr) - 1)
/*----------------------------------------------------------------------------*/

/*
 * strict unsigned binary ascii block to u16
 * caller must provide a valid memory block
 * inplace conversion: ok
 */
ULINUX_EXPORT bool ulinux_bin2u16_blk(ulinux_u16 *dest, ulinux_u8 *start,
								ulinux_u8 *last)
{/* do *not* trust content */
	if ((ulinux_u64)(last - start) >= CSTRLEN("1111111111111111"))
		return false;

	*dest = 0;
	loop {
		if (start > last)
			break;

		if (*last != '0' && *last != '1')
			return false;
		*dest = (*dest << 1) | (*start - '0');
		++start;
	}
	/*no overflow*/
	return true;
}

/*----------------------------------------------------------------------------*/
/* local cleanup*/
#undef loop
#undef CSTRLEN
/*----------------------------------------------------------------------------*/
#undef ULINUX_EXPORT
#endif
