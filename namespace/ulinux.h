/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define loop for(;;)
#define constant_u32 enum
#define sl ulinux_sl
#define si ulinux_si
#define ul ulinux_ul
#define u8 ulinux_u8
#define s8 ulinux_s8
#define u16 ulinux_u16
#define s16 ulinux_s16
#define s32 ulinux_s32
#define u32 ulinux_u32
#define f32 ulinux_f32
#define s64 ulinux_s64
#define u64 ulinux_u64
#define f64 ulinux_f64
/*----------------------------------------------------------------------------*/
#define accept4(a,b,c,d) ulinux_sysc_4(accept4,a,(ul)b,(ul)c,d)
#define AF_INET ULINUX_AF_INET
#define AF_INET6 ULINUX_AF_INET6
#define ANONYMOUS ULINUX_MAP_ANONYMOUS
#define bind(a,b,c) ulinux_sysc_3(bind,a,(ul)b,c)
#define CLOCK_MONOTONIC ULINUX_CLOCK_MONOTONIC
#define close(a) ulinux_sysc_1(close, a)
#define cpu_to_be16_const ulinux_cpu_to_be16_const
#define dec2u32_blk ulinux_dec2u32_blk
#define dec2u8_blk ulinux_dec2u8_blk
#define EAGAIN ULINUX_EAGAIN
#define EBADF ULINUX_EBADF
#define EINTR ULINUX_EINTR
#define epoll_create1(a) ulinux_sysc_1(epoll_create1,a)
#define epoll_ctl(a,b,c,d) ulinux_sysc_4(epoll_ctl,a,b,c,(ul)d)
#define EPOLL_CTL_ADD ULINUX_EPOLL_CTL_ADD
#define EPOLL_CTL_DEL ULINUX_EPOLL_CTL_DEL
#define EPOLL_CTL_MOD ULINUX_EPOLL_CTL_MOD
#define epoll_event ulinux_epoll_event
#define epoll_pwait(a,b,c,d,e) ulinux_sysc_5(epoll_pwait,a,(ul)b,c,d,e)
#define EPOLLET ULINUX_EPOLLET
#define EPOLLERR ULINUX_EPOLLERR
#define EPOLLHUP ULINUX_EPOLLHUP
#define EPOLLIN ULINUX_EPOLLIN
#define EPOLLOUT ULINUX_EPOLLOUT
#define EPOLLPRI ULINUX_EPOLLPRI
#define EWOULDBLOCK ULINUX_EWOULDBLOCK
#define exit(a) ulinux_sysc_1(exit_group,a)
#define EWOULDBLOCK ULINUX_EWOULDBLOCK
#define ISERR ULINUX_ISERR
#define itimerspec ulinux_itimerspec
#define listen(a,b) ulinux_sysc_2(listen,a,b)
#define memcmp ulinux_memcmp
#define memcpy ulinux_memcpy_forward
#define memset ulinux_memset
#define mmap(a,b,c) ulinux_sysc_6(mmap,0,a,b,c,0,0)
#define O_RDONLY ULINUX_O_RDONLY
#define openat(a,b,c,d) ulinux_sysc_4(openat,a,(ul)b,c,d)
#define PRIVATE ULINUX_MAP_PRIVATE
#define RD ULINUX_PROT_READ
#define read(a,b,c) ulinux_sysc_3(read,a,(ul)b,c)
#define rt_sigprocmask(a,b,c,d) ulinux_sysc_4(rt_sigprocmask,a,(ul)b,c,d)
#define setsockopt(a,b,c,d,e) ulinux_sysc_5(setsockopt,a,b,c,(ul)d,e)
#define signalfd_siginfo ulinux_signalfd_siginfo
#define SFD_NONBLOCK ULINUX_SFD_NONBLOCK
#define SIG_BLOCK ULINUX_SIG_BLOCK
#define signalfd4(a,b,c,d) ulinux_sysc_4(signalfd4,a,(ul)b,c,d)
#define SIGTERM ULINUX_SIGTERM
#define SO_REUSEADDR ULINUX_SO_REUSEADDR
#define SOCK_CLOEXEC ULINUX_SOCK_CLOEXEC
#define SOCK_STREAM ULINUX_SOCK_STREAM
#define SOCK_NONBLOCK ULINUX_SOCK_NONBLOCK
#ifdef CONFIG_IPV4
#define sockaddr_in ulinux_sockaddr_in
#else
#define sockaddr_in6 ulinux_sockaddr_in6
#endif
#define SOL_SOCKET ULINUX_SOL_SOCKET
/* we don't handle i386, will add it if I am asked for it */
#define socket(a,b,c) ulinux_sysc_3(socket,a,b,c)
#define TFD_CLOEXEC ULINUX_TFD_CLOEXEC
#define TFD_NONBLOCK ULINUX_TFD_NONBLOCK
#define timerfd_create(a,b) ulinux_sysc_2(timerfd_create,a,b)
#define timerfd_settime(a,b,c,d) ulinux_sysc_4(timerfd_settime,a,b,(ul)c,d)
#define timerspec ulinux_timerspec
#define vsnprintf ulinux_vsnprintf
#define WR ULINUX_PROT_WRITE
#define write(a,b,c) ulinux_sysc_3(write,a,(ul)b,c)
/******************************************************************************/
#else /* CLEANUP */
#undef loop
#undef constant_u32
#undef sl
#undef si
#undef ul
#undef u8
#undef s8
#undef u16
#undef s16
#undef s32
#undef u32
#undef f32
#undef s64
#undef u64
#undef f64
/*----------------------------------------------------------------------------*/
#undef accept4
#undef AF_INET
#undef AF_INET6
#undef ANONYMOUS
#undef bind
#undef CLOCK_MONOTONIC
#undef close
#undef cpu_to_be16_const
#undef dec2u32_blk
#undef dec2u8_blk
#undef EAGAIN
#undef EBADF
#undef EINTR
#undef epoll_create
#undef epoll_ctl
#undef EPOLL_CTL_ADD
#undef EPOLL_CTL_DEL
#undef EPOLL_CTL_MOD
#undef epoll_event
#undef epoll_pwait
#undef EPOLLET
#undef EPOLLERR
#undef EPOLLHUP
#undef EPOLLIN
#undef EPOLLOUT
#undef EPOLLPRI
#undef exit
#undef EWOULDBLOCK
#undef ISERR
#undef itimerspec
#undef listen
#undef memcmp
#undef memcpy
#undef memset
#undef mmap
#undef O_RDONLY
#undef openat
#undef PRIVATE
#undef RD
#undef read
#undef rt_sigprocmask
#undef setsockopt
#undef signalfd_siginfo
#undef SFD_NONBLOCK
#undef SIG_BLOCK
#undef signalfd4
#undef SIGTERM
#undef SO_REUSEADDR
#undef SOCK_CLOEXEC
#undef SOCK_STREAM
#undef SOCK_NONBLOCK
#undef SOL_SOCKET
#ifdef CONFIG_IPV4
#undef sockaddr_in
#else
#undef sockaddr_in6
#endif
#undef socket
#undef TFD_CLOEXEC
#undef TFD_NONBLOCK
#undef timerfd_create
#undef timerfd_settime
#undef timerspec
#undef vsnprintf
#undef WR
#undef write
#endif
