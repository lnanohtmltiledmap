#ifndef ULINUX_UTILS_ASCII_BLOCK_CONV_NET_NET_H
#define ULINUX_UTILS_ASCII_BLOCK_CONV_NET_NET_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */

/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT extern 
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/

ULINUX_EXPORT bool ulinux_to_ipv4_blk(ulinux_u32 *dest, ulinux_u8 *start,
                                                               ulinux_u8 *last);
ULINUX_EXPORT bool ulinux_to_ipv6_blk(ulinux_u8 *dest, ulinux_u8 *start,
							ulinux_u8 *last);
#undef ULINUX_EXPORT
#endif
