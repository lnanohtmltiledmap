#!/bin/sh


# XXX: the compiler driver is brained damaged: have a specific build script for each toolchain.


top_dir=$(realpath "$(dirname "$0")")
printf "TOP_DIR=$top_dir\n"
src_dir=$(realpath "$top_dir/lnanohtmltiledmap")
printf "SRC_DIR=$src_dir\n"
build_dir="$top_dir/build"
printf "BUILD_DIR=$build_dir\n"

cd "$build_dir"

# install our configuration file
cp "$top_dir/config.h" "$build_dir/config.h"

# configure ulinux for arm64
rm "$build_dir/ulinux/arch"
rmdir "$build_dir/ulinux"
mkdir "$build_dir/ulinux"
ln -s "$src_dir/ulinux/archs/aarch64" "$build_dir/ulinux/arch"

# The DEBUG definition, will output "debug stuff" on the stderr (file descriptor 2)
DEBUG=
#DEBUG=-DDEBUG

# cpp the assembler source file and the C source file
gcc -E -o "$build_dir/all.s" \
	$DEBUG \
	\
	-DULINUX_ASSEMBLY_SYSCALL \
	-DULINUX_ASSEMBLY_ENDIAN \
	-D__GNUAS__ \
	\
	-nostdinc \
	"-I$build_dir" \
	"-I$src_dir" \
	\
	"$src_dir/all.S"

# we must know where the toolchain compiler stores its include files
gcc -E -o "$build_dir/all.cpp.c" \
	$DEBUG \
	\
	-DULINUX_ASSEMBLY_SYSCALL \
	-DULINUX_ASSEMBLY_ENDIAN \
	\
	-nostdinc \
	-isystem /usr/lib/gcc/aarch64-linux-gnu/12/include-fixed \
	-isystem /usr/lib/gcc/aarch64-linux-gnu/12/include \
	"-I$build_dir" \
	"-I$src_dir" \
	\
	"$src_dir/all.c" 

# generate the assembler from the C file cpp-ed file
gcc -S -o "$build_dir/all.cpp.c.s" \
	-ffreestanding \
	-pipe -O2 -fpie \
	"$build_dir/all.cpp.c"

# assemble
as -o "$build_dir/all.cpp.c.s.o" \
	"$build_dir/all.cpp.c.s"
as -o "$build_dir/all.s.o" \
	"$build_dir/all.s"

# link the file
ld -pie -s -o "$build_dir/lnanohtmltiledmap" \
	-nostdlib \
	"$build_dir/all.cpp.c.s.o" \
	"$build_dir/all.s.o"

