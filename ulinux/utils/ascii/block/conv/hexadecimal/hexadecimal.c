#ifndef ULINUX_UTILS_ASCII_BLOCK_CONV_HEXADECIMAL_HEXADECIMAL_C
#define ULINUX_UTILS_ASCII_BLOCK_CONV_HEXADECIMAL_HEXADECIMAL_C
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#include <stdbool.h>

#include <ulinux/compiler_types.h>
#include <ulinux/types.h>

#include <ulinux/utils/ascii/ascii.h>

/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* local */
#define loop for(;;)
#define CSTRLEN(cstr) (sizeof("ffff") - 1)
/*----------------------------------------------------------------------------*/

/* caller must provide a valid memory block */
ULINUX_EXPORT bool ulinux_hex_to_u16_blk(ulinux_u16 *dest, ulinux_u8 *start,
								ulinux_u8 *last)
{/* do *not* trust content */
	if ((ulinux_u64)(last - start + 1) > CSTRLEN("ffff"))
		return false;

	*dest = 0;
	loop {
		ulinux_u8 base;

		if (start > last)
			break;

		base = ulinux_is_hex(*start);
		if (base == 0)
			return false;

		*dest = (*dest << 4) + (*start - base);
		if ((base & 0x40) != 0) /* 'A' or 'a' are 0x41 and '0x61' */
			*dest += 0x0a;
		++start;
	}
	/* no overflow */
	return true;
}

/*----------------------------------------------------------------------------*/
/* local cleanup */
#undef loop
#undef CSTRLEN
/*----------------------------------------------------------------------------*/
#undef ULINUX_EXPORT
#endif
