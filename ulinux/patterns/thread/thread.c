#include <stdarg.h>

#include <ulinux/compiler_types.h>
#include <ulinux/sysc.h>

#include <ulinux/types.h>
#include <ulinux/sched.h>
#include <ulinux/mmap.h>

#include <ulinux/utils/ascii/string/vsprintf.h>

/* ulinux namespace */
#define EINTR ULINUX_EINTR
#define EAGAIN ULINUX_EAGAIN
#define si ulinux_si
#define sl ulinux_sl
#define u8 ulinux_u8
#define exit(a) ulinux_sysc(exit,1,a)
#define mmap(a,b,c,d,e,f) ulinux_sysc(mmap,6,a,b,c,d,e,f)
#define RD ULINUX_PROT_READ
#define WR ULINUX_PROT_WRITE
#define PRIVATE ULINUX_MAP_PRIVATE
#define ANONYMOUS ULINUX_MAP_ANONYMOUS
#define GROWSDOWN ULINUX_MAP_GROWSDOWN
#define clone(a,b,c,d,e) ulinux_sysc(clone,5,a,b,c,d,e)
#define THREAD ULINUX_CLONE_THREAD
#define SIGHAND ULINUX_CLONE_SIGHAND
#define VM ULINUX_CLONE_VM
#define FILES ULINUX_CLONE_FILES
#define FS ULINUX_CLONE_FS
#define IO ULINUX_CLONE_IO
#define SYSVSEM ULINUX_CLONE_SYSVSEM
#define PAGE_SZ ULINUX_PAGE_SZ

#define loop for(;;)
#define STACK_SZ 512 * PAGE_SZ

#define BUFSIZ 8192
static u8 dprint_buf[BUFSIZ];
#define POUT(fmt,...) ulinux_dprintf(1, &dprint_buf[0], BUFSIZ - 1, fmt, ##__VA_ARGS__)

/*
 * XXX: be carefull that this c++ cr*p which is now gcc don't optimize
 * out/weirdly the thread entry function
 */
static void thread_entry(void)
{
	POUT("CHILD\n");
	loop; 
	exit(0);
}

void _start(void)
{
	sl r;
	sl stack_map_bottom;
	sl stack_map_top; /* 1 byte out of map */
	sl stack_map_top_down_aligned;

	dprint_buf[BUFSIZ - 1] = 0; /* secure a 0 terminating char */

	stack_map_bottom = mmap(0, STACK_SZ, RD | WR, PRIVATE | ANONYMOUS
							| GROWSDOWN, -1, 0);
	stack_map_top = stack_map_bottom + STACK_SZ;

	POUT("THREAD STACK MAP:0x%p-0x%p STACK_MAP_SZ=0x%x PAGE_SZ=0x%x\n",
			stack_map_bottom, stack_map_top, STACK_SZ, PAGE_SZ);

	/*
	 * we align DOWN on a 16 bytes boundary. Fits aarch64 and x86_64
	 * ("function stack frames" should be too)
	 */
	if ((stack_map_top & 0xf) != 0)
		stack_map_top_down_aligned = stack_map_top & (~0xf);
	else
		stack_map_top_down_aligned = (stack_map_top  - 1) & (~0xf);
	POUT("16 bytes aligned thread stack at 0x%p\n",
						stack_map_top_down_aligned);

	/*
	 * XXX: carefull, the order of the syscall args varies from arch to
	 * arch. If you create a process (namely without the VM flag), you
	 * don't need to provide a stack, just put 0 (copy-on-write)
	 */
	r = clone(THREAD | SIGHAND | VM | FILES | FS | IO | SYSVSEM,
					stack_map_top_down_aligned, 0, 0, 0);
	if (r == 0)
		thread_entry();
	POUT("PARENT\n");
	loop;
	exit(0);
}
