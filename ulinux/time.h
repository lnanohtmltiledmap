#ifndef ULINUX_TIME_H
#define ULINUX_TIME_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
struct ulinux_timespec {
	ulinux_sl sec;	/*seconds*/
	ulinux_sl nsec;	/*nanoseconds*/
};

struct ulinux_timeval {
	ulinux_sl sec;	/*seconds*/
	ulinux_sl usec;	/*micro seconds, type can be arch dependent (i.e. an int)*/
};

struct ulinux_itimerspec {
	struct ulinux_timespec interval;/* timer period */
	struct ulinux_timespec value;	/* timer expiration */
};

#define ULINUX_CLOCK_REALTIME			0
#define ULINUX_CLOCK_MONOTONIC			1
#define ULINUX_CLOCK_PROCESS_CPUTIME_ID		2
#define ULINUX_CLOCK_THREAD_CPUTIME_ID		3
#define ULINUX_CLOCK_MONOTONIC_RAW		4
#define ULINUX_CLOCK_REALTIME_COARSE		5
#define ULINUX_CLOCK_MONOTONIC_COARSE		6
#define ULINUX_CLOCK_BOOTTIME			7
#define ULINUX_CLOCK_REALTIME_ALARM		8
#define ULINUX_CLOCK_BOOTTIME_ALARM		9
/* hole */
#define ULINUX_MAX_CLOCKS			16

#define ULINUX_CLOCKS_MASK			(ULINUX_CLOCK_REALTIME | ULINUX_CLOCK_MONOTONIC)
#define ULINUX_CLOCKS_MONO			ULINUX_CLOCK_MONOTONIC

#define ULINUX_TIMER_ABSTIME			0x01

/*
 * XXX: monitor closely those flags because they may collide with others and be
 * updated
 */
#define ULINUX_TFD_TIMER_ABSTIME (1 << 0)
#define ULINUX_TFD_TIMER_CANCEL_ON_SET (1 << 1)

#ifndef ULINUX_O_CLOEXEC
#error "missing definition of ULINUX_O_CLOEXEC"
#endif
#define ULINUX_TFD_CLOEXEC ULINUX_O_CLOEXEC

#ifndef ULINUX_O_NONBLOCK
#error "missing definition of ULINUX_O_NONBLOCK"
#endif
#define ULINUX_TFD_NONBLOCK ULINUX_O_NONBLOCK
#endif
