/*
 * COMMON ABBREVIATIONS:
 * dyn : dynamic
 * ehdr : Extended HeaDeR
 * hdr : HeaDeR
 * idx(s) : InDeX(S)
 * phdr(s) : Program segment HeaDeR(S)
 * ptr(s) : PoinTeR(S)
 * seg(s): program SEGment(S)
 * spec(s) : specification(s)
 * tab(s) : TABLe(S)
 * val(s) : VALue(S)
 * rvaddr : Real Virual ADDRess
 */
#include <stdarg.h>
#include <stdbool.h>

#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
#include <ulinux/sysc.h>

#include <ulinux/compiler_misc.h>

#include <ulinux/start.h>
#include <ulinux/elf.h>

#include <ulinux/utils/ascii/string/vsprintf.h>

#define ul ulinux_ul
#define u8 ulinux_u8
#define u16 ulinux_u16
#define u32 ulinux_u32
#define u64 ulinux_u64
/* kill process, aka thread group */
#define exit(code) ulinux_sysc(exit_group,1,code)
#define elf64_auxv ulinux_elf64_auxv
#define AT_NULL ULINUX_AT_NULL
#define AT_SYSINFO_EHDR ULINUX_AT_SYSINFO_EHDR
/* elf tabs */
#define PT_LOAD ULINUX_PT_LOAD
#define PT_DYNAMIC ULINUX_PT_DYNAMIC
#define elf64_ehdr ulinux_elf64_ehdr
#define elf64_phdr ulinux_elf64_phdr
#define DT_NULL ULINUX_DT_NULL
#define DT_HASH ULINUX_DT_HASH
#define DT_STRTAB ULINUX_DT_STRTAB
#define DT_SYMTAB ULINUX_DT_SYMTAB
#define DT_VERSYM ULINUX_DT_VERSYM
#define DT_VERDEF ULINUX_DT_VERDEF
#define elf64_dyn ulinux_elf64_dyn
#define STN_UNDEF ULINUX_STN_UNDEF
#define elf64_sym ulinux_elf64_sym

/* convenience macros */
#define BUFSIZ 8192
static u8 dprint_buf[BUFSIZ];
#define POUT(fmt,...) ulinux_dprintf(1,&dprint_buf[0],BUFSIZ-1,fmt,##__VA_ARGS__)

#define loop for(;;)

#if BITS_PER_LONG != 64
#error "sorry, only elf64 code"
#endif

/*============================================================================*/
/* see linux_src/tools/testing/selftests/vDSO for reference parsing */

static u8 *vdso;
static u64 vdso_load_offset;
/*----------------------------------------------------------------------------*/
/* the following are the "dynamic" version elf sections */
static u32 *vdso_hash; 		/* hash hdr is 2 32 bits words */
static u32 *vdso_hash_buckets;	/* 32 bits words, each one is a chain idx */
static u32 vdso_hash_buckets_n;	/* first 32 bits word of the hdr */
static u32 *vdso_hash_chains;	/* 32 bit words, each one is a symtab idxs */
static u32 vdso_hash_chains_n;	/* second 32 bits word of the hdr */
/*----------------------------------------------------------------------------*/
/* the following are the "dynamic" version elf sections */
static u8 *vdso_strtab;
static struct elf64_sym *vdso_symtab;
static u8 *vdso_versym;
static u8 *vdso_verdef;

static void vdso_sym_dump(u32 idx)
{
	POUT("dynamic symbol entry idx 0x%lx at 0x%p\n", idx, &vdso_symtab[idx]);
	POUT("\tname=%s\n\n", vdso_strtab + vdso_symtab[idx].name);
}

static void vdso_hash_scan(void)
{
	u32 bucket;
	u32 chain;

	POUT("----------------\n");
	POUT("scanning hash table at 0x%p for dynamic symbols in dynamic section at 0x%p\n", vdso_hash, vdso_symtab);

	bucket = 0;
	chain = STN_UNDEF;
	loop {
		u32 chain;

		if (bucket == vdso_hash_buckets_n)
			break;
		POUT("----\n");
		POUT("bucket %lu:\n\n", bucket);

		chain = vdso_hash_buckets[bucket];
		loop {
			if (chain == STN_UNDEF)
				break;
			/*
			 * the chain value is an index in the dynamic symbol
 			 * table _AND_ the chain table as well
			 */
			vdso_sym_dump(chain);

			/* next in chain */
			chain = vdso_hash_chains[chain];
		}

		++bucket;
	}
}

static void vdso_dyns(struct elf64_dyn *dyn)
{
	POUT("----------------\n");
	POUT("parsing dynamic elf section, or elf dynamic entries starting at 0x%p\n", dyn);

	loop {
		if (dyn->tag == DT_NULL) {
			break;
		} else if (dyn->tag == DT_STRTAB) {
			POUT("found string table dynamic entry at vaddr 0x%lx\n", dyn->ptr);

			vdso_strtab = (u8*)(dyn->ptr + vdso_load_offset);
		} else if (dyn->tag == DT_SYMTAB) {
			POUT("found symbol table dynamic entry at vaddr 0x%lx\n", dyn->ptr);

			vdso_symtab = (struct elf64_sym*)(dyn->ptr
							+ vdso_load_offset);
		} else if (dyn->tag == DT_VERSYM) {
			POUT("found version symbol table dynamic entry at vaddr 0x%lx\n", dyn->ptr);
		} else if (dyn->tag == DT_VERDEF) {
			POUT("found version definition table dynamic entry at vaddr 0x%lx\n", dyn->ptr);
		} else if (dyn->tag == DT_HASH) {
	
			vdso_hash = (u32*)(dyn->ptr + vdso_load_offset);
			vdso_hash_buckets_n = vdso_hash[0];
			vdso_hash_chains_n = vdso_hash[1];
			vdso_hash_buckets = &vdso_hash[2];
			vdso_hash_chains =
					&vdso_hash_buckets[vdso_hash_buckets_n];
			POUT("found dynamic symbol hash at vaddr 0x%lx, %u buckets %u chains\n", dyn->ptr, vdso_hash_buckets_n, vdso_hash_chains_n);
		} else {
			POUT("found dynamic entry type 0x%x\n", dyn->tag);
		}
		++dyn;
	}

	vdso_hash_scan();
}

/*
 * the vdso is in a bastard ("PRE-LINKED") state. there is no rigorous
 * definition. You have to follow what you get from the gnu linker script and
 * how it is mapped in linux arch specific code. In glibc (2.29), you can find
 * the castrated, but kind of generic, loading procedure in
 * glibc_src/elf/setup-vdso.c
 */
static void vdso_auxv(void)
{
	struct elf64_ehdr *ehdr;
	struct elf64_phdr *phdrs;
	u16 i;
	bool load_seg_found;
	bool dyn_seg_found;
	u64 dyns;	/* will be the rvaddr of the "dynamic" elf section */

	/* XXX: the vdso pointer is actually where the sole "load" elf program
	 * segment is mapped ("pre-linked"). from the gnu linker script
	 * we know we have the elf64 hdrs at the start of this memory map
	 * (should we have a "PHDRS" elf program segment?  the gnu linker
	 * script does not do that at the time of writting for x86_64)
	 */
	ehdr = (struct elf64_ehdr*)vdso;
	POUT("parsing the vdso elf extended header at memory 0x%p for high performance system calls...\n", ehdr);
	phdrs = (struct elf64_phdr*)(vdso + ehdr->phoff);

	load_seg_found = false;
	dyn_seg_found = false;
	i = 0;
	loop {
		if (i == ehdr->phnum)
			break;

		/*
		 * XXX: only "load" elf program segments are actually mapped
		 * into memory. the elf program segments of other types are
		 * loaded into "load" elf program segments: they overlap.
		 * The "load" elf program segments are mapped in memory in
		 * ascending order (specs). the real vaddr of the first
		 * "load" elf program segment does matter.
		 */
		if ((phdrs[i].type == PT_LOAD) && !load_seg_found) {
			POUT("found first \"load\" program segment[%u]:\n", i);
			POUT("\tfile offset=0x%lx\n", phdrs[i].offset);
			POUT("\tfile size=0x%lx\n", phdrs[i].filesz);
			POUT("\tvaddr=0x%lx\n", phdrs[i].vaddr);
			POUT("\tmemory size=0x%lx\n", phdrs[i].memsz);
			POUT("\tflags=0x%lx\n", phdrs[i].flags);
			POUT("\talign=0x%lx\n", phdrs[i].align);

			load_seg_found = true;

			/*
 			 * XXX: from the table vaddr to real vaddr:
			 * rvaddr = vaddr + offset
			 * then offset = rvaddr - vaddr
			 * the first "load" elf program segment was loaded at vdso.
			 * cheeky unsigned arithmetic (warp around)
			 */
			vdso_load_offset = (u64)vdso - phdrs[i].vaddr;
			
		} else if (phdrs[i].type == PT_DYNAMIC) { 
			/*
			 * XXX: the "dynamic" program segment, is already mapped
			 * into the sole and first "load" program segment. it
			 * contains the "dynamic" elf section, by ABI
			 * definition.
			 */
			POUT("found \"dynamic\" program segment[%u]:\n", i);
			POUT("\tfile offset=0x%lx\n", phdrs[i].offset);
			POUT("\tfile size=0x%lx\n", phdrs[i].filesz);
			POUT("\tvaddr=0x%lx\n", phdrs[i].vaddr);
			POUT("\tmemory size=0x%lx\n", phdrs[i].memsz);
			POUT("\tflags=0x%lx\n", phdrs[i].flags);
			POUT("\talign=0x%lx\n", phdrs[i].align);

			dyn_seg_found = true;

			dyns = phdrs[i].vaddr;
		} else {
			POUT("found program segment[%u] type 0x%x\n", i, phdrs[i].type);
			POUT("\tfile offset=0x%lx\n", phdrs[i].offset);
			POUT("\tfile size=0x%lx\n", phdrs[i].filesz);
			POUT("\tvaddr=0x%lx\n", phdrs[i].vaddr);
			POUT("\tmemory size=0x%lx\n", phdrs[i].memsz);
			POUT("\tflags=0x%lx\n", phdrs[i].flags);
			POUT("\talign=0x%lx\n", phdrs[i].align);
		}
		++i;
	}

	if (!load_seg_found) {
		POUT("not load elf program segment found\n");
		return;
	}

	if (!dyn_seg_found) {
		POUT("not dynamic elf program segment, hence dynamic elf section, found\n");
		return;
	}

	/* XXX: compute the rvaddr with the offset we now have */
	dyns += vdso_load_offset;
	vdso_dyns((struct elf64_dyn*)dyns);
}
/* vdso */
/*============================================================================*/

/* shows what arch ABIs, linux ABI, put on the initial stack */
void ulinux_start(u8 *stack) 
{
	ul *argc;
	u8 **argv;
	u8 **envp;
	struct elf64_auxv *auxv;
	ul arg_idx;
	
	dprint_buf[BUFSIZ - 1] = 0; /* secure a 0 terminating char */

	/* argc */
	POUT("----------------\n");
	argc = (ul*)stack;
	POUT("argc=%lu\n", *argc);

	/* skip argc */
	stack += sizeof(*argc);

	/* argv */
	POUT("----------------\n");
	argv = (u8**)stack;
	arg_idx = 0;
	loop {
		if (*argv == 0)
			break;
		POUT("arg[%lu]=%s\n", arg_idx, *argv);
		++argv;
		++arg_idx;
	}

	/* skip ending 0 pointer */
	++argv;
	stack = (u8*)argv;

	/* envp */
	POUT("----------------\n");
	envp = (u8**)stack;
	loop {
		if (*envp == 0)
			break;
		POUT("envp:%s\n", *envp);
		++envp;
	}

	/* skip ending 0 pointer */
	++envp;
	stack = (u8*)envp;

	/* auxv */
	POUT("----------------\n");
	auxv = (struct elf64_auxv*)stack;
	loop {
		if (auxv->type == AT_NULL)
			break;
		POUT("auxv:0x%p:type=%lu:val=0x%lx", auxv, auxv->type, auxv->val);
		if (auxv->type == AT_SYSINFO_EHDR) {
			POUT(":VDSO\n");
			vdso = (u8*)auxv->val;
		} else
			POUT("\n");
		++auxv;
	}

	/* vdso auxv, linux ABI specific */
	POUT("----------------\n");
	vdso_auxv();
	exit(0);
}
