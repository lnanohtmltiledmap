#ifndef ULINUX_SOCKET_TCP_H
#define ULINUX_SOCKET_TCP_H
/* the socket option level */
#define ULINUX_IPPROTO_TCP 6

/* the socket options */
#define ULINUX_TCP_USER_TIMEOUT 18 /* How long for loss retry before timeout */
#endif
