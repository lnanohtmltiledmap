/*
 * XXX: if you are a heavy user of of time related calls, vdso calls
 * are prefered to syscalls. That depends on the arch.
 */
#include <stdarg.h>

#include <ulinux/compiler_types.h>
#include <ulinux/sysc.h>

#include <ulinux/error.h>
#include <ulinux/types.h>

#include <ulinux/time.h>

#include <ulinux/utils/mem.h>
#include <ulinux/utils/ascii/string/vsprintf.h>

/* ulinux namespace */
#define EINTR ULINUX_EINTR
#define EAGAIN ULINUX_EAGAIN
#define si ulinux_si
#define sl ulinux_sl
#define u8 ulinux_u8
#define exit(a) ulinux_sysc(exit,1,a)
#define ISERR(x) ULINUX_ISERR(x)
#define CLOCK_MONOTONIC ULINUX_CLOCK_MONOTONIC
#define TIMER_ABSTIME ULINUX_TIMER_ABSTIME
#define clock_getres(a,b) ulinux_sysc(clock_getres,2,a,b)
#define clock_gettime(a,b) ulinux_sysc(clock_gettime,2,a,b)
#define clock_nanosleep(a,b,c,d) ulinux_sysc(clock_nanosleep,4,a,b,c,d)
#define memset(a,b,c) ulinux_memset((u8*)a,b,c)
#define timespec ulinux_timespec

#define loop for(;;)

#define BUFSIZ 8192
static u8 dprint_buf[BUFSIZ];
#define POUT(fmt,...) ulinux_dprintf(1, &dprint_buf[0], BUFSIZ - 1, fmt, ##__VA_ARGS__)

#define SLEEP_TIME_SEC 8

static void get_monotonic_clock(struct timespec *request)
{
	sl r;

	memset(request, 0, sizeof(*request));

	r = clock_gettime(CLOCK_MONOTONIC, request);
	if (ISERR(r)) {
		POUT("error while getting the current monotonic clock time (%ld)\n", r);
		exit(2);
	}
	POUT("current monotonic clock is %ld secs and %ld nsecs\n", request->sec, request->nsec);
}

void _start(void)
{
	sl r;
	struct timespec resolution;
	struct timespec target;
	struct timespec current_time;
	
	dprint_buf[BUFSIZ - 1] = 0; /* secure a 0 terminating char */

	memset(&resolution, 0, sizeof(resolution));

	r = clock_getres(CLOCK_MONOTONIC, &resolution);

	if (ISERR(r)) {
		POUT("error while getting the resolution of the monotonic clock (%ld)\n", r);
		exit(1);
	}

	POUT("monotonic clock resolution is %lu|%lu sec|nsec\n", resolution.sec, resolution.nsec);

	/*-------------------------------------------------------------------*/

	get_monotonic_clock(&target);

	/*-------------------------------------------------------------------*/

	/* project ourself of a few secs in the future of this absolute clock */
	target.sec += SLEEP_TIME_SEC; 
	target.nsec = 0;
	POUT("targeting monotonic clock at %ld secs and %ld nsecs\n", target.sec, target.nsec);

	/*
	 * we put the call in a loop because it should be restarted if the call
	 * was interrupted my a interrupt
	 */
	loop { 
		/* ABSolute timer */
		r = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &target, 0);
		if (r == 0)
			break;

		if (r == -EINTR) {
			POUT("interrupted by a runned signal handler at: ");
			get_monotonic_clock(&current_time);
			POUT("restarting, targeting the same absolute clock time\n");
			continue;
		}

		if (ISERR(r)) {
			POUT("error while nanosleeping (%ld) at:\n", r);
			get_monotonic_clock(&current_time);
			exit(4);
		}
	}
	POUT("successfully waited and we are at: ");
	get_monotonic_clock(&current_time);
	exit(0);
}
