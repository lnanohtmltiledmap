/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#include "ulinux/utils/mem.c"
#include "ulinux/utils/ascii/string/vsprintf.c"
#include "ulinux/utils/ascii/block/conv/decimal/decimal.c"
#include "math.c"
#include "utils.c"
#include "html.c"
#include "http.c"
