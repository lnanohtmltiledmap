#ifndef ULINUX_UTILS_MEM_C
#define ULINUX_UTILS_MEM_C
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#include <stdbool.h>
#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/
/* local */
#define loop for(;;)
#define u8 ulinux_u8
#define u64 ulinux_u64
/*----------------------------------------------------------------------------*/
ULINUX_EXPORT void ulinux_memcpy_forward(void *d, void *s, u64 len)
{
	u8 *to;
	u8 *from;
	
	to = d;
	from = s;
	loop {
		if (len == 0)
			break;
		*to++ = *from++;
		len--;
	}
}
ULINUX_EXPORT void ulinux_memset(void *d, u8 c, u64 len)
{
	u8 *cur;

	cur = d;
	loop {
		if (len == 0)
			break;
		*cur++ = c;
		len--;
	}
}

ULINUX_EXPORT bool ulinux_memcmp(void *d, void *c, u64 len)
{
	u8 *su1;
	u8 *su2;

	su1 = d;
	su2 = c;
	loop {
		if (len == 0)
			return true;

		if (*su1 != *su2)
			return false;
			
		++su1;
		++su2;
		len--;
	}
}
/*----------------------------------------------------------------------------*/
#if defined __TINYC__ && defined TINYC_GENERATED_MEMORY_OPERATIONS
#include <stddef.h> /* size_t */
void *memmove(void *d, void *s, size_t n)
{
	if (d == s)
		return d;
	if ((d - s) >= n) { /* carefull: this is pointer modular arithmetics */
		ulinux_memcpy_forward(d, s, (u64)n);
		return d;
	}
	loop {
		if (n == 0)
			return d;
		n--;
		d[n] = s[n];
	}
}
void *memcpy(void *d, void *s, size_t n)
{
	ulinux_memcpy_forward(d, s, (u64)n);
	return d;
}
void *memset(void *d, int c, size_t n)
{
	ulinux_memset(d, (u8)c, (u64)n);
	return d;
}
#endif /* __TINYC__ && TINYC_GENERATED_MEMORY_OPERATIONS */
/*----------------------------------------------------------------------------*/
#undef loop
#undef u8
#undef u64
#undef ULINUX_EXPORT
/*----------------------------------------------------------------------------*/
#endif
