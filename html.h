#ifndef LNANOHTMLTILEDMAP_HTML_H
#define LNANOHTMLTILEDMAP_HTML_H
#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
/*============================================================================*/
#include "namespace/ulinux.h"
/*============================================================================*/
constant_u32 {
	HTML_QUERY_NAV_CMD_GEO_MODIFY,
	HTML_QUERY_NAV_CMD_TRANSLATION_NORTH,
	HTML_QUERY_NAV_CMD_TRANSLATION_SOUTH,
	HTML_QUERY_NAV_CMD_TRANSLATION_WEST,
	HTML_QUERY_NAV_CMD_TRANSLATION_EAST,
	HTML_QUERY_NAV_CMD_ZOOM_MODIFY,
	HTML_QUERY_NAV_CMD_ZOOM_PLUS,
	HTML_QUERY_NAV_CMD_ZOOM_MINUS
};
struct html_query_t {
	f64 lo;
	f64 la;
	u8 zoom;
	u32 x;
	u32 y;
	/* ---- */
	u8 nav_cmd;
};
struct html_ctx_t {
	f64 lo;
	f64 la;
	u8 zoom;
	u64 x;
	u64 y;
	/* ---- */
	u8 *p;
	u64 urandom;
	u8 verts_n; 
	u8 horizs_n;
};
static void html_query_init(struct html_query_t *q);
static void html_gen_ctx_init_from_query(struct html_ctx_t *c,
				struct html_query_t *q, u8 *buf, u64 urandom);
static void html_majortbl_gen(struct html_ctx_t *c);
static u32 html_sz_max(void);
static void html_query_decode(struct html_query_t *q, u8 *s, u8 *e);
/*============================================================================*/
#define CLEANUP
#include "namespace/ulinux.h"
#undef CLEANUP
/*============================================================================*/
#endif
