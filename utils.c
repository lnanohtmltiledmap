#ifndef LNANOHTMLTILEDMAP_UTILS_C
#define LNANOHTMLTILEDMAP_UTILS_C
#include <stdbool.h>
#include <ulinux/compiler_types.h>
#include <ulinux/types.h>

#include "common_cpp.h"
/*============================================================================*/
#include "namespace/ulinux.h"
/*============================================================================*/
union f64_t {
	f64 f;
	u64 x;
};
/*
 * naive f64 to/from text for longitude/latitude, 3 integer decimal digits, 
 * 9 fractional decimal digits
 * does not add a terminating 0
 * return the number of bytes which would have been required for the 
 * conversion, even if the buf was too small, namely if the output is
 * truncated (snprintf way-ish)
 * ulinux printf is from the kernel: no floating point.
 */ 
#define INTEGER_DIGITS_N_MAX	3
#define FRACTIONAL_DIGITS_N_MAX	9
#define FRACTIONAL_TO_INTEGER 1e9
static u8 f64_to_txt(f64 num, u8 *buf, u8 buf_sz)
{
	f64 integer_f64;
	s64 integer;
	union f64_t fractional_f64;
	u64 fractional;
	u8 integer_digits[INTEGER_DIGITS_N_MAX];
	u8 fractional_digits[FRACTIONAL_DIGITS_N_MAX];
	u8 *buf_e;
	bool negative;
	u8 bytes_n;
	u8 i;
	u8 *first_valid_digit;
	u8 *last_valid_digit;
	u8 valid_digits_n;

	fractional_f64.f = modf(num, &integer_f64);
	integer = (s64)integer_f64; /* compiler based conversion */

	if (integer < 0) {
		/* modf will return both parts as negative */
		negative = true;
		integer = -integer;
		fractional_f64.x &= 0x7fffffffffffffff; /* remove sign bit */
	} else
		negative = false;

	bytes_n = 0;
	buf_e = buf + buf_sz;
	/*--------------------------------------------------------------------*/
	if (negative) {
		if (buf < buf_e) {
			*buf = '-';
			++buf;
		}
		++bytes_n;
	}
	/*--------------------------------------------------------------------*/
	if (integer != 0) {
		i = INTEGER_DIGITS_N_MAX - 1;
		loop  {
			u8 digit;

			digit = integer % 10;
			integer_digits[i] = digit + 0x30;
			integer = integer / 10;
			if (integer == 0 || i == 0)
				break;
			i--;
		}
		first_valid_digit = &integer_digits[i];
	} else {
		integer_digits[INTEGER_DIGITS_N_MAX - 1] = '0';
		first_valid_digit = &integer_digits[INTEGER_DIGITS_N_MAX - 1];
	}
	valid_digits_n = (u8)(&integer_digits[INTEGER_DIGITS_N_MAX - 1]
						- first_valid_digit + 1);
	if ((buf + valid_digits_n) < buf_e) {
		memcpy(buf, first_valid_digit, valid_digits_n);
		buf += valid_digits_n;
	}
	bytes_n += valid_digits_n;
	/*--------------------------------------------------------------------*/
	if (buf < buf_e) {
		*buf = '.';
		++buf;
	} 
	++bytes_n;
	/*--------------------------------------------------------------------*/
	/* we do that to get nice fractional digits */
	fractional_f64.f *= FRACTIONAL_TO_INTEGER;
	fractional_f64.f = round(fractional_f64.f);
	fractional = (u64)fractional_f64.f;
	if (fractional != 0) {
		i = FRACTIONAL_DIGITS_N_MAX - 1;
		loop  {
			u8 digit;

			digit = fractional % 10;
			fractional_digits[i] = digit + 0x30;
			fractional = fractional / 10;
			if (fractional == 0 || i == 0)
				break;
			i--;
		}
		if (i >= 1) /* fill the missing 0s */
			memset(fractional_digits, '0', i);
		last_valid_digit =
				&fractional_digits[FRACTIONAL_DIGITS_N_MAX - 1];
		loop {
			if (*last_valid_digit != '0'
				|| last_valid_digit == &fractional_digits[0])
				break;
			last_valid_digit--;
		}
	} else {
		fractional_digits[0] = '0';
		last_valid_digit = &fractional_digits[0];
	}
	valid_digits_n = (u8)(last_valid_digit - &fractional_digits[0] + 1);
	if ((buf + valid_digits_n) < buf_e) {
		memcpy(buf, fractional_digits, valid_digits_n);
		buf += valid_digits_n;
	}
	bytes_n += valid_digits_n;
	return bytes_n;
}
#undef INTEGER_DIGITS_N_MAX
#undef FRACTIONAL_DIGITS_N_MAX
#undef FRACTIONAL_TO_INTEGER
/* truncated or not, will add a terminating 0 */
static void f64_to_str(f64 num, u8 *buf, u8 buf_sz)
{
	u8 sz;

	sz = f64_to_txt(num, buf, buf_sz);
	if (sz >= buf_sz)
		sz = buf_sz;
	buf[sz] = 0;
}
/* naive and simple text to float */
static void dec_to_f64(f64 *f, u8 *s, u8 *l)
{
	u8 *radix;
	u8 *p;
	f64 exp;
	bool negative;
	
	radix = s;
	loop {
		if (radix == (l + 1) || *radix == '.')
			break;
		++radix;
	}
	*f = 0.0;
	exp = 1.0;
	p = radix - 1;
	negative = false;
	loop {
		if (p < s)
			break;
		if (*p == '-') {
			negative = true;
			break;
		}
		*f += (f64)(*p - 0x30) * exp;
		exp *= 10.0;
		p--;
	}
	exp = 0.1;
	p = radix + 1;
	loop {
		if (p > l)
			break;
		*f += (f64)(*p - 0x30) * exp;
		exp *= 0.1;
		++p;
	}
	if (negative) {
		union f64_t *pf;
		pf = (union f64_t*)f;
		pf->x |= 0x8000000000000000;
	}
}
/*============================================================================*/
#define CLEANUP
#include "common_cpp.h"
#include "namespace/ulinux.h"
#undef CLEANUP
/*============================================================================*/
#endif
