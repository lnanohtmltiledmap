#ifndef LNANOHTMLTILEDMAP_HTML_C
#define LNANOHTMLTILEDMAP_HTML_C
#include <stdbool.h>
#include <stdarg.h>
#include <ulinux/compiler_types.h>
#include <ulinux/types.h>

#include <ulinux/utils/ascii/string/vsprintf.h>
#include <ulinux/utils/ascii/block/conv/decimal/decimal.h>

#include "config.h"
#include "html.h"
/*============================================================================*/
#include "common_cpp.h"
#include "namespace/ulinux.h"
/*============================================================================*/
/*
 * decimal longitude/latitude: sign + integer part = 3 decimal digits (180)
 * 	+ radix + fractional part = 9 digits (micron/overkill precision)
 */
#define GEO_DECIMAL_MAX "-123.123456789"
#define U32_DECIMAL_DIGITS_N_MAX "4294967295"
#define U64_HEXA_DIGITS_N_MAX "XXXXXXXXXXXXXXXX"
/*
 * this is an html template
 * p suffix means prolog/header
 * e suffix means epilog/trailer
 */
/* we cheat: we generate the http response headers here */
#define MAJORTBL_P "\
HTTP/1.1 200 OK\r\n\
cache-control:no-cache\r\n\
connection:close\r\n\
content-type:text/html;charset=utf-8\r\n\
\r\n\
<html>\n\
<body>\n\
	<table rules=\"all\" id=\"major_layout_table\">\n\
		<tbody>\n\
			<tr>\n\
				<td id=\"tiles_cell\" align=\"center\">\n"
#define VIEWTBL_P "\
					<table rules=\"all\" id=\"tiles_layout_table\" cellpadding=\"0\" cellspacing=\"0\">\n\
						<tbody>\n"
/* min longitude, max longitude */
#define VIEWTBL_VERTHDRS_P "\
							<tr>\n"
#define VIEWTBL_VERTHDRS_WEST "\
								<th scope=\"col\" align=\"left\">%s</th>\n"
#define VIEWTBL_VERTHDRS_EAST "\
								<th scope=\"col\" align=\"right\">%s</th>\n"
#define VIEWTBL_VERTHDRS_E "\
								<th/>\n\
							</tr>\n"
#define VIEWTBL_MAINROW_P "\
							<tr>\n"
/* span horizontal headers rows and vertical header columns */
#define VIEWTBL_TILE "\
								<td colspan=\"2\" rowspan=\"2\" align=\"center\" valign=\"middle\">\n\
									<img src=\"" CONFIG_TILE_SVR "\"/>\n\
								</td>\n"
/* min latitude */
#define VIEWTBL_MAINROW_HORIZTRAILER_CELL_TOP "\
								<th valign=\"top\">%s</th>\n"
#define VIEWTBL_MAINROW_E "\
							</tr>\n"
/* max latitude */
#define VIEWTBL_HORIZTRAILER_ROW_BOT "\
							<tr>\n\
								<th valign=\"bottom\">%s</th>\n\
							</tr>\n"
#define VIEWTBL_E "\
						</tbody>\n\
					</table>\n"
#define MAJORTBL_VIEWTBL_TO_NAVFORM "\
				</td>\n\
			</tr>\n\
			<tr>\n\
				<td id=\"navigation_cell\" align=\"center\">\n\
					<form action=\".\" method=\"get\">\n"
#define URANDOM "\
						<input type=\"hidden\" name=\"urandom\" value=\"%u\"/>\n"
#define NAV_X "\
						<input type=\"hidden\" name=\"x\" value=\"%u\"/>\n"
#define NAV_Y "\
						<input type=\"hidden\" name=\"y\" value=\"%u\"/>\n"
#define NAVTBL_P "\
						<table rules=\"all\">\n\
							<tbody>\n\
								<tr>\n\
									<td colspan=\"2\" align=\"center\">\n\
										<fieldset>\n\
											<legend><em>center coordinates</em></legend>\n"
#define NAVTBL_LO "\
											<label>longitude = <input name=\"lo\" type=\"text\" maxlength=\"14\" value=\"%s\"/></label>\n"
#define NAVTBL_LA "\
											<label>latitude = <input name=\"la\" type=\"text\" maxlength=\"14\" value=\"%s\"/></label>\n"
#define NAVTBL_MIDDLE "\
											<button name=\"nav\" value=\"geo_modify\">MODIFY</button>\n\
										</td>\n\
									</fieldset>\n\
								</tr>\n\
								<tr>\n\
									<td align=\"center\">\n\
										<fieldset>\n\
											<legend><em>translation</em></legend>\n\
											<table>\n\
												<tbody>\n\
													<tr>\n\
														<td colspan=\"2\" align=\"center\">\n\
															<button name=\"nav\" value=\"translation_north\">NORTH</button>\n\
														</td>\n\
													</tr>\n\
													<tr>\n\
														<td>\n\
															<button name=\"nav\" value=\"translation_west\">WEST</button>\n\
														</td>\n\
														<td>\n\
															<button name=\"nav\" value=\"translation_east\">EAST</button>\n\
														</td>\n\
													</tr>\n\
													<tr>\n\
														<td colspan=\"2\" align=\"center\">\n\
															<button name=\"nav\" value=\"translation_south\">SOUTH</button>\n\
														</td>\n\
													</tr>\n\
												</tbody>\n\
											</table>\n\
										</fieldset>\n\
									</td>\n\
									<td align=\"center\">\n\
										<fieldset>\n\
											<legend><em>zoom</em></legend>\n"
#define NAVTBL_ZOOM "\
											<label>level = <input name=\"zoom\" type=\"text\" maxlength=\"2\" value=\"%u\"/></label>\n"
#define NAVTBL_E "\
											<button name=\"nav\" value=\"zoom_modify\">MODIFY</button>\n\
											<button name=\"nav\" value=\"zoom_plus\">PLUS</button>\n\
											<button name=\"nav\" value=\"zoom_minus\">MINUS</button>\n\
										</fieldset>\n\
									</td>\n\
								</tr>\n\
							</tbody>\n\
						</table>\n"
#define MAJORTBL_E "\
					</form>\n\
				</td>\n\
			</tr>\n\
		</tbody>\n\
	</table>\n\
</body>\n\
</html>"
/* maximum sizes of template parts */
#define VIEWTBL_VERTHDRS_WEST_SZ_MAX \
(SZ(VIEWTBL_VERTHDRS_WEST) - SZ("%s") + SZ(GEO_DECIMAL_MAX))

#define VIEWTBL_VERTHDRS_EAST_SZ_MAX \
(SZ(VIEWTBL_VERTHDRS_EAST) - SZ("%s") + SZ(GEO_DECIMAL_MAX))

#define VIEWTBL_TILE_SZ_MAX \
(SZ(VIEWTBL_TILE) - (SZ("%c") + CONFIG_HORIZS_N_MAX * SZ("%u")) \
						+ SZ(CONFIG_TILE_SVR_MAX))

#define VIEWTBL_MAINROW_HORIZTRAILER_CELL_TOP_SZ_MAX \
(SZ(VIEWTBL_MAINROW_HORIZTRAILER_CELL_TOP) - SZ("%s") + SZ(GEO_DECIMAL_MAX))

#define VIEWTBL_HORIZTRAILER_ROW_BOT_SZ_MAX \
(SZ(VIEWTBL_HORIZTRAILER_ROW_BOT) - SZ("%s") + SZ(GEO_DECIMAL_MAX))

#define URANDOM_SZ_MAX \
(SZ(URANDOM) - SZ("%u") + SZ(U64_HEXA_DIGITS_N_MAX))

#define NAV_X_SZ_MAX \
(SZ(NAV_X) - SZ("%u") + SZ(U32_DECIMAL_DIGITS_N_MAX))

#define NAV_Y_SZ_MAX \
(SZ(NAV_Y) - SZ("%u") + SZ(U32_DECIMAL_DIGITS_N_MAX))

#define NAVTBL_LO_SZ_MAX \
(SZ(NAVTBL_LO) - SZ("%s") + SZ(GEO_DECIMAL_MAX))

#define NAVTBL_LA_SZ_MAX \
(SZ(NAVTBL_LA) - SZ("%s") + SZ(GEO_DECIMAL_MAX))

#define NAVTBL_ZOOM_SZ_MAX \
(SZ(NAVTBL_ZOOM) - SZ("%u") + SZ("99"))
static void tmpl_to_html(struct html_ctx_t *c, u8 *tmpl, u64 tmpl_sz_max, ...)
{
	va_list ap;
	u64 sz;

	va_start(ap, tmpl_sz_max);
	sz = vsnprintf(c->p, tmpl_sz_max, tmpl, ap);
	va_end(ap);
	if (sz > tmpl_sz_max)
		++(c->p); /* ERROR: empty string */
	else
		c->p += sz;
}
/* lo or la, longitude or latitude */
static void tmpl_lx_to_html(struct html_ctx_t *c, u8 *tmpl, u64 tmpl_sz_max,
									f64 lx)
{
	u8 lx_str[SZ(GEO_DECIMAL_MAX) + 1];

	f64_to_str(lx, lx_str, sizeof(lx_str));
	tmpl_to_html(c, tmpl, tmpl_sz_max, lx_str);
}
#define TMPL_LX_TO_HTML(c,tmpl,lx) \
	tmpl_lx_to_html(c, tmpl, tmpl##_SZ_MAX, lx)
#define TMPL_TO_HTML(c,tmpl,...) \
	tmpl_to_html(c,tmpl,tmpl##_SZ_MAX, __VA_ARGS__)
#define TXT_TO_HTML(x) \
	memcpy(c->p, x, SZ(x));\
	c->p += SZ(x);
/* XXX: should have the best 64bits float hex value (double) for PI approx */
#define PI 3.141592653589793238462643383279502884
static void html_geo_to_tile_center(struct html_ctx_t *c)
{
	f64 la_rad; /* latitude in radian */
	f64 arsinh_tan_la_rad;
	/* exact location in "web mercator" coords */
	f64 x_exact;	
	f64 y_exact;
	/*
	 * degree longitude: from -180 to +180, east -> positive,
	 * west -> negative
	 * tile coordinate: x coordinate starts at the internation date line
	 * (longitude * 180) degrees and goes from east to west. x >= 0.
	 * zoom: its power of 2 is the number of divisions of the raw degree
	 * longitude.
	 *
	 * degree latitute: from 90 to -90, north -> positive
	 * south -> negative
	 * tile coordinate: y coordinate starts an the north pole, then toward
	 * the south pole. y >= 0.
	 * zoom: it the power of 2 of the number of divisions of the "web
	 * mercator" (spherical->cylindrical) latitude projection  
	 */
	x_exact = (c->lo + 180.0) / 360.0 * (f64)(1 << c->zoom);
	c->x = (u64)floor(x_exact);
	la_rad = c->la * PI / 180.0;
	/* arsinh(tan(α)) = ln((1 + sin(α)) / cos(α)) */
	arsinh_tan_la_rad = log((1 + sin(la_rad)) / cos(la_rad));
	y_exact = (1.0 - (arsinh_tan_la_rad / PI)) / 2.0
							* (f64)(1 << c->zoom);
	c->y = (u64)floor(y_exact);
}
static void html_geo_modify(struct html_ctx_t *c, struct html_query_t *q)
{
	c->lo = q->lo;
	c->la = q->la;
	c->zoom = q->zoom;
	html_geo_to_tile_center(c);
}
static void html_tile_center_to_geo(struct html_ctx_t *c)
{
	f64 x_center;
	f64 y_center;
	f64 tmp;

	x_center = (f64)(c->x) + 0.5;
	c->lo = (x_center * 360.0) / (f64)(1 << c->zoom) - 180.0;

	y_center = (f64)(c->y) + 0.5;
	tmp = PI - 2.0 * PI * y_center / (f64)(1 << c->zoom);
	c->la = 180.0 / PI * atan(0.5 * (exp(tmp) - exp(-tmp)));
}
static void html_translate_north(struct html_ctx_t *c, struct html_query_t *q)
{
	c->x = q->x;
	c->y = (q->y - 1) % (u32)(1 << q->zoom);
	c->zoom = q->zoom;

	html_tile_center_to_geo(c);
}
static void html_translate_south(struct html_ctx_t *c, struct html_query_t *q)
{
	c->x = q->x;
	c->y = (q->y + 1) % (u32)(1 << q->zoom);
	c->zoom = q->zoom;

	html_tile_center_to_geo(c);
}
static void html_translate_west(struct html_ctx_t *c, struct html_query_t *q)
{
	c->x = (q->x - 1) % (u32)(1 << q->zoom);
	c->y = q->y;
	c->zoom = q->zoom;

	html_tile_center_to_geo(c);
}
static void html_translate_east(struct html_ctx_t *c, struct html_query_t *q)
{
	c->x = (q->x + 1) % (u32)(1 << q->zoom);
	c->y = q->y;
	c->zoom = q->zoom;

	html_tile_center_to_geo(c);
}
static void html_zoom_plus(struct html_ctx_t *c, struct html_query_t *q)
{
	c->lo = q->lo;
	c->la = q->la;
	c->zoom = q->zoom + 1;
	html_geo_to_tile_center(c);
}
static void html_zoom_minus(struct html_ctx_t *c, struct html_query_t *q)
{
	c->lo = q->lo;
	c->la = q->la;
	if (q->zoom != 0)
		c->zoom = q->zoom - 1;
	else
		c->zoom = q->zoom;
	html_geo_to_tile_center(c);
}
static void html_zoom_modify(struct html_ctx_t *c, struct html_query_t *q)
{
	html_geo_modify(c, q);
}
static void html_gen_ctx_init_from_query(struct html_ctx_t *c,
				struct html_query_t *q, u8 *buf, u64 urandom)
{
	switch (q->nav_cmd) {
	case HTML_QUERY_NAV_CMD_TRANSLATION_NORTH:
		html_translate_north(c, q);
		break;
	case HTML_QUERY_NAV_CMD_TRANSLATION_SOUTH:
		html_translate_south(c, q);
		break;
	case HTML_QUERY_NAV_CMD_TRANSLATION_WEST:
		html_translate_west(c, q);
		break;
	case HTML_QUERY_NAV_CMD_TRANSLATION_EAST:
		html_translate_east(c, q);
		break;
	case HTML_QUERY_NAV_CMD_ZOOM_PLUS:
		html_zoom_plus(c, q);
		break;
	case HTML_QUERY_NAV_CMD_ZOOM_MINUS:
		html_zoom_minus(c, q);
		break;
	case HTML_QUERY_NAV_CMD_ZOOM_MODIFY:
		html_zoom_modify(c, q);
		break;
	case HTML_QUERY_NAV_CMD_GEO_MODIFY:
	default:
		html_geo_modify(c, q);
		break;
	}
	c->verts_n = CONFIG_VERTS_N_MAX;
	c->horizs_n = CONFIG_HORIZS_N_MAX;
	c->urandom = urandom;
	c->p = buf;
}
static void html_viewtbl_verthdrs_gen(struct html_ctx_t *c)
{
	u8 vert;
	
	TXT_TO_HTML(VIEWTBL_VERTHDRS_P);
	/*--------------------------------------------------------------------*/
	vert = 0;
	loop {
		f64 x;
		f64 lo_west;
		f64 lo_east;

		if (vert == c->verts_n)
			break;
		x = (f64)((c->x - (u64)c->verts_n / 2 + (u64)vert)
							% (1 << c->zoom));
		lo_west = (x * 360.0) / (f64)(1 << c->zoom) - 180.0;
		lo_east = ((x + (CONFIG_TILE_PIXS_N_MAX - 1.0)
			/ CONFIG_TILE_PIXS_N_MAX) * 360.0) / (f64)(1 << c->zoom)
									- 180.0;
		TMPL_LX_TO_HTML(c, VIEWTBL_VERTHDRS_WEST, lo_west);
		TMPL_LX_TO_HTML(c, VIEWTBL_VERTHDRS_EAST, lo_east);
		++vert;
	}
	/*--------------------------------------------------------------------*/
	TXT_TO_HTML(VIEWTBL_VERTHDRS_E);
}
static void html_viewtbl_tile(struct html_ctx_t *c, u8 horiz)
{
	u8 vert;

	vert = 0;
	loop {
		u64 x;
		u64 y;

		if (vert == c->verts_n)
			break;
		x = (c->x - (u64)c->verts_n / 2 + (u64)vert) % (1 << c->zoom);
		y = (c->y - (u64)c->horizs_n / 2 + (u64)horiz) % (1 << c->zoom);
		TMPL_TO_HTML(c, VIEWTBL_TILE, config_row_svrs[horiz % 3],
								c->zoom, x, y);
		++vert;
	}
}
static void html_viewtbl_mainrow_horiztrailer_cell_top(struct html_ctx_t *c,
								u8 horiz)
{
	f64 y;
	f64 tmp;
	f64 la;

	y = (f64)((c->y - (u64)c->horizs_n / 2 + (u64)horiz) % (1 << c->zoom));
	tmp = PI - 2.0 * PI * y / (f64)(1 << c->zoom);
	la = 180.0 / PI * atan(0.5 * (exp(tmp) - exp(-tmp)));
	TMPL_LX_TO_HTML(c, VIEWTBL_MAINROW_HORIZTRAILER_CELL_TOP,la);
}
static void html_viewtbl_mainrow_gen(struct html_ctx_t *c, u8 horiz)
{
	TXT_TO_HTML(VIEWTBL_MAINROW_P);
	html_viewtbl_tile(c, horiz);
	html_viewtbl_mainrow_horiztrailer_cell_top(c, horiz);
	TXT_TO_HTML(VIEWTBL_MAINROW_E);
}
static void html_viewtbl_horiztrailer_row_bot(struct html_ctx_t *c, u8 horiz)
{
	f64 y;
	f64 tmp;
	f64 la;
	
	y = (f64)((c->y - (u64)c->horizs_n / 2 + (u64)horiz) % (1 << c->zoom));
	tmp = PI - 2.0 * PI * (y + (CONFIG_TILE_PIXS_N_MAX - 1.0)
				/ CONFIG_TILE_PIXS_N_MAX) / (f64)(1 << c->zoom);
	la = 180.0 / PI * atan(0.5 * (exp(tmp) - exp(-tmp)));
	TMPL_LX_TO_HTML(c, VIEWTBL_HORIZTRAILER_ROW_BOT, la);
}
static void html_viewtbl_gen(struct html_ctx_t *c)
{
	u8 horiz;

	TXT_TO_HTML(VIEWTBL_P);
	html_viewtbl_verthdrs_gen(c);
	horiz = 0;
	loop {
		if (horiz == c->horizs_n)
			break;
		html_viewtbl_mainrow_gen(c, horiz);
		html_viewtbl_horiztrailer_row_bot(c, horiz);
		++horiz;
	}
	TXT_TO_HTML(VIEWTBL_E);
}
static void html_navform_navtbl_gen(struct html_ctx_t *c)
{
	TXT_TO_HTML(NAVTBL_P);
	TMPL_LX_TO_HTML(c, NAVTBL_LO, c->lo);
	TMPL_LX_TO_HTML(c, NAVTBL_LA, c->la);
	TXT_TO_HTML(NAVTBL_MIDDLE);
	TMPL_TO_HTML(c, NAVTBL_ZOOM, c->zoom);
	TXT_TO_HTML(NAVTBL_E);
}
static void html_navform_gen(struct html_ctx_t *c)
{
	TMPL_TO_HTML(c, URANDOM, c->urandom);
	TMPL_TO_HTML(c, NAV_X, c->x);
	TMPL_TO_HTML(c, NAV_Y, c->y);
	html_navform_navtbl_gen(c);
}
static void html_majortbl_gen(struct html_ctx_t *c)
{
	TXT_TO_HTML(MAJORTBL_P);
	html_viewtbl_gen(c);
	TXT_TO_HTML(MAJORTBL_VIEWTBL_TO_NAVFORM);
	html_navform_gen(c);
	TXT_TO_HTML(MAJORTBL_E);
}
static u32 html_sz_max(void)
{
	return \
	SZ(MAJORTBL_P) +
		SZ(VIEWTBL_P) +
			SZ(VIEWTBL_VERTHDRS_P) +
			CONFIG_VERTS_N_MAX * (
					VIEWTBL_VERTHDRS_WEST_SZ_MAX +
					VIEWTBL_VERTHDRS_EAST_SZ_MAX) +
			SZ(VIEWTBL_VERTHDRS_E) +
			CONFIG_HORIZS_N_MAX * (
				SZ(VIEWTBL_MAINROW_P) +
				CONFIG_VERTS_N_MAX * VIEWTBL_TILE_SZ_MAX +
				VIEWTBL_MAINROW_HORIZTRAILER_CELL_TOP_SZ_MAX +
				SZ(VIEWTBL_MAINROW_E) +
				VIEWTBL_HORIZTRAILER_ROW_BOT_SZ_MAX) +
		SZ(VIEWTBL_E) +
	SZ(MAJORTBL_VIEWTBL_TO_NAVFORM) +
		URANDOM_SZ_MAX +
		NAV_X_SZ_MAX +
		NAV_Y_SZ_MAX +
		SZ(NAVTBL_P) +
			NAVTBL_LO_SZ_MAX +
			NAVTBL_LA_SZ_MAX +
			SZ(NAVTBL_MIDDLE) +
			NAVTBL_ZOOM_SZ_MAX +
		SZ(NAVTBL_E) +
	SZ(MAJORTBL_E);
}
#define NAV_IS(x) memcmp(x, s, e - s)
static void html_control_nav(struct html_query_t *q, u8 *s, u8 *e)
{
	if (s == e)
		return;
	if	(NAV_IS("geo_modify")) /* default if nav is missing */
		q->nav_cmd = HTML_QUERY_NAV_CMD_GEO_MODIFY;
	else if	(NAV_IS("translation_north"))
		q->nav_cmd = HTML_QUERY_NAV_CMD_TRANSLATION_NORTH;
	else if	(NAV_IS("translation_south"))
		q->nav_cmd = HTML_QUERY_NAV_CMD_TRANSLATION_SOUTH;
	else if	(NAV_IS("translation_west"))
		q->nav_cmd = HTML_QUERY_NAV_CMD_TRANSLATION_WEST;
	else if	(NAV_IS("translation_east"))
		q->nav_cmd = HTML_QUERY_NAV_CMD_TRANSLATION_EAST;
	else if	(NAV_IS("zoom_plus"))
		q->nav_cmd = HTML_QUERY_NAV_CMD_ZOOM_PLUS;
	else if	(NAV_IS("zoom_minus"))
		q->nav_cmd = HTML_QUERY_NAV_CMD_ZOOM_MINUS;
	else if	(NAV_IS("zoom_modify"))
		q->nav_cmd = HTML_QUERY_NAV_CMD_ZOOM_MODIFY;
}
#undef NAV_IS
static void html_control_x(struct html_query_t *q, u8 *s, u8 *e)
{
	u32 x;

	if (s == e)
		return;
	if (dec2u32_blk(&x, s, e - 1))
		q->x = x;
}
static void html_control_y(struct html_query_t *q, u8 *s, u8 *e)
{
	u32 y;

	if (s == e)
		return;
	if (dec2u32_blk(&y, s, e - 1))
		q->y = y;
}
static void html_control_zoom(struct html_query_t *q, u8 *s, u8 *e)
{
	u8 z;

	if (s == e)
		return;
	if (dec2u8_blk(&z, s, e - 1))
		q->zoom = z;
}
static void html_control_la(struct html_query_t *q, u8 *s, u8 *e)
{
	f64 la;

	dec_to_f64(&la, s, e - 1); /* only integers in ulinux */
	if (-90.0 <= la && la <= 90.0)
		q->la = la;
}
static void html_control_lo(struct html_query_t *q, u8 *s, u8 *e)
{
	f64 lo;

	dec_to_f64(&lo, s, e - 1); /* only integeres in ulinux */
	if (-180.0 <= lo && lo <= 180.0)
		q->lo = lo;
}
#define NAME_IS(x) memcmp(x, name, name_e - name)
static void html_control_decode(struct html_query_t *q, u8 *name, u8 *name_e,
							u8 *value, u8 *value_e)
{
	if (name == name_e)
		return;
	if	(NAME_IS("nav"))
		html_control_nav(q, value, value_e);
	else if	(NAME_IS("x"))
		html_control_x(q, value, value_e);
	else if	(NAME_IS("y"))
		html_control_y(q, value, value_e);
	else if (NAME_IS("zoom"))
		html_control_zoom(q, value, value_e);
	else if (NAME_IS("lo"))
		html_control_lo(q, value, value_e);
	else if (NAME_IS("la"))
		html_control_la(q, value, value_e);
}
#undef NAME_IS
static void html_query_decode(struct html_query_t *q, u8 *p, u8 *e)
{ loop {
	u8 *name;
	u8 *name_e;
	u8 *value;
	u8 *value_e;

	if (p == e)
		return;
	name = p;
	name_e = p;
	value = p;
	value_e = p;
	/* name */
	loop {
		if (p == e || *p == '=' || *p == '&') {
			name_e = p;
			break;
		}
		++p;
	}
	if (*p == '=') { /* value if any */
		++p; /* skip '=' */
		value = p;
		loop {
			if (p == e || *p == '&') {
				value_e = p;
				break;
			}
			++p;
		}
	}
	if (*p == '&')
		++p; /* skip '&' */
	html_control_decode(q, name, name_e, value, value_e);
}}
static void html_query_init(struct html_query_t *q)
{
	/* sane init values */
	q->zoom = 0;
	q->lo = 0.0;
	q->la = 0.0;
	q->x = 0;
	q->y = 0;
	/* will ignore query x and y */
	q->nav_cmd = HTML_QUERY_NAV_CMD_GEO_MODIFY;
}
/*----------------------------------------------------------------------------*/
#undef GEO_DECIMAL_MAX
#undef U32_DECIMAL_DIGITS_N_MAX
#undef U64_HEXA_DIGITS_N_MAX
#undef PI
#undef MAJORTBL_P
#undef VIEWTBL_P
#undef VIEWTBL_VERTHDRS_P
#undef VIEWTBL_VERTHDRS_WEST
#undef VIEWTBL_VERTHDRS_WEST_SZ_MAX
#undef VIEWTBL_VERTHDRS_EAST
#undef VIEWTBL_VERTHDRS_EAST_SZ_MAX
#undef VIEWTBL_VERTHDRS_E
#undef VIEWTBL_MAINROW_P
#undef VIEWTBL_TILE
#undef VIEWTBL_TILE_SZ_MAX
#undef VIEWTBL_MAINROW_HORIZTRAILER_CELL_TOP
#undef VIEWTBL_MAINROW_HORIZTRAILER_CELL_TOP_SZ_MAX
#undef VIEWTBL_MAINROW_E
#undef VIEWTBL_HORIZTRAILER_ROW_BOT
#undef VIEWTBL_HORIZTRAILER_ROW_BOT_SZ_MAX
#undef VIEWTBL_E
#undef MAJORTBL_VIEWTBL_TO_NAVFORM
#undef URANDOM
#undef URANDOM_SZ_MAX
#undef NAV_X
#undef NAV_X_SZ_MAX
#undef NAV_Y
#undef NAV_Y_SZ_MAX
#undef NAVTBL_P
#undef NAVTBL_LO
#undef NAVTBL_LO_SZ_MAX
#undef NAVTBL_LA_SZ
#undef NAVTBL_LA_SZ_MAX
#undef NAVTBL_MIDDLE
#undef NAVTBL_ZOOM
#undef NAVTBL_E
#undef MAJORTBL_E
#define CLEANUP
#include "common_cpp.h"
/*============================================================================*/
#include "namespace/ulinux.h"
/*============================================================================*/
#undef CLEANUP
#endif
