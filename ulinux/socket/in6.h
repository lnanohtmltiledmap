#ifndef ULINUX_SOCKET_IN6_H
#define ULINUX_SOCKET_IN6_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
struct ulinux_sockaddr_in6 {
	ulinux_u16 family;    	/* AF_INET6 */
	ulinux_u16 port;
	ulinux_u32 flowinfo;
	ulinux_u8 addr[16];	/* ipv6 address */
	/* scope id (new in RFC2553), endian not defined */
	ulinux_u32 scope_id;
};
#endif
